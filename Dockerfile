FROM openjdk:17-alpine
EXPOSE 8088
COPY target/browse-builds-service-0.0.1-SNAPSHOT.jar browse-builds-service-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java","-jar","/browse-builds-service-0.0.1-SNAPSHOT.jar" ]