package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.InvalidInvitationException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.InvitationNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.InvitationUsedException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserVerifiedException;
import com.mariarif.browsebuildsservice.applicationservices.services.InvitationService;
import com.mariarif.browsebuildsservice.domain.model.Invitation;
import com.mariarif.browsebuildsservice.infrastructure.security.TokenProvider;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.InvitationRepository;

public class InvitationServiceImpl implements InvitationService {
	@Autowired
	private final InvitationRepository invitationRepository;

	@Autowired
	private final TokenProvider tokenProvider;
//	
//	@Autowired
//	private final MailService mailService;

	public InvitationServiceImpl(InvitationRepository invitationRepository, TokenProvider tokenProvider) {
		this.invitationRepository = invitationRepository;
		this.tokenProvider = tokenProvider;
	}

	@Override
	public void useInvitation(String invitationToken) throws BaseException {
		if (tokenProvider.validateInvitationToken(invitationToken)) {
			Invitation invitation = invitationRepository.findByToken(invitationToken)
					.orElseThrow(() -> new InvitationNotFoundException(invitationToken));
			if (invitation.getUsed()) {
				throw new InvitationUsedException();
			}
			if (invitation.getUser().getEmailVerified()) {
				throw new UserVerifiedException(invitation.getUser().getId());
			}
			invitation.getUser().setEmailVerified(true);
			invitation.setUsed(true);
			invitationRepository.save(invitation);
		} else
			throw new InvalidInvitationException(invitationToken);
	}
}
