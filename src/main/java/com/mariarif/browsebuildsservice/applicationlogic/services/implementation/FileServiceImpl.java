package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.mariarif.browsebuildsservice.applicationservices.services.FileService;
import com.mariarif.browsebuildsservice.infrastructure.configuration.property.FileStorageProperties;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileServiceImpl implements FileService {
	private final Path fileStorageLocation;
	private BlobServiceClient blobServiceClient;

	@Autowired
	public FileServiceImpl(@NonNull FileStorageProperties fileStorageProperties, BlobServiceClient blobServiceClient) {
		fileStorageLocation = Path.of(fileStorageProperties.getUploadDir());
		this.blobServiceClient = blobServiceClient;
		try {
			Files.createDirectories(fileStorageLocation);
		} catch (IOException e) {
			log.error("Could not create the directory where the uploaded files will be stored.", e);
		}
	}

	public Path getFileStorageLocation() {
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (IOException e) {
			log.error("Could not create the directory where the uploaded files will be stored.", e);
		}
		return fileStorageLocation;
	}

	public String uploadFile(@NonNull MultipartFile file, String containerName) throws IOException {
		BlobContainerClient blobContainerClient = getBlobContainerClient(containerName);
		String filename = generateFilename(file);
		BlockBlobClient blockBlobClient = blobContainerClient.getBlobClient(filename).getBlockBlobClient();
		try {
			if (blockBlobClient.exists()) {
				blockBlobClient.delete();
			}
			blockBlobClient.upload(new BufferedInputStream(file.getInputStream()), file.getSize(), true);
			return blockBlobClient.getBlobUrl();
		} catch (IOException e) {
			log.error("Error while uploading file {}", e.getLocalizedMessage());
			throw e;
		}
	}

	private String generateFilename(MultipartFile file) {
		return file.getOriginalFilename() + '-' + String.valueOf(file.getSize()) + '-' + file.getContentType();
	}

	public Boolean uploadAndDownloadFile(@NonNull MultipartFile file, String containerName) {
		boolean isSuccess = true;
		BlobContainerClient blobContainerClient = getBlobContainerClient(containerName);
		String filename = file.getOriginalFilename();
		BlockBlobClient blockBlobClient = blobContainerClient.getBlobClient(filename).getBlockBlobClient();
		try {
			if (blockBlobClient.exists()) {
				blockBlobClient.delete();
			}
			blockBlobClient.upload(new BufferedInputStream(file.getInputStream()), file.getSize(), true);
			blockBlobClient.getBlobUrl();
			String tempFilePath = fileStorageLocation + "/" + filename;
			Files.deleteIfExists(Paths.get(tempFilePath));
			blockBlobClient.downloadToFile(new File(tempFilePath).getPath());
		} catch (IOException e) {
			isSuccess = false;
			log.error("Error while processing file {}", e.getLocalizedMessage());
		}
		return isSuccess;
	}

	private @NonNull BlobContainerClient getBlobContainerClient(@NonNull String containerName) {
		BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(containerName);

		if (!blobContainerClient.exists()) {
			blobContainerClient.create();
		}
		return blobContainerClient;
	}
}
