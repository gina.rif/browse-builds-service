package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.io.InvalidObjectException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.MessageNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.services.CategoryService;
import com.mariarif.browsebuildsservice.applicationservices.services.MailService;
import com.mariarif.browsebuildsservice.applicationservices.services.MessageService;
import com.mariarif.browsebuildsservice.applicationservices.services.UserService;
import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.domain.model.Message;
import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.MessageRepository;

public class MessageServiceImpl implements MessageService {
	@Autowired
	private final MessageRepository messageRepository;

	@Autowired
	private final CategoryService categoryService;

	@Autowired
	private final UserService userService;

	@Autowired
	private final MailService mailService;

	public MessageServiceImpl(MessageRepository messageRepository, CategoryService categoryService,
			UserService userService, MailService mailService) {
		this.messageRepository = messageRepository;
		this.categoryService = categoryService;
		this.userService = userService;
		this.mailService = mailService;
	}

	@Override
	public Message createMessage(Message message, long fromUserId, long toUserId, long[] categoriesIds)
			throws BaseException, InvalidObjectException {
		if (fromUserId == toUserId) {
			throw new InvalidObjectException("Sender user and receiver user cannot be the same!");
		}
		User fromUser = userService.getUserById(fromUserId);
		if (!fromUser.getEmailVerified()) {
			throw new BaseException("Email not verified for sender user!");
		}
		User toUser = userService.getUserById(toUserId);
		if (!toUser.getEmailVerified()) {
			throw new BaseException("Email not verified for receiver user!");
		}
		fromUser.addMessageSent(message);
		toUser.addMessageReceived(message);

		if (categoriesIds.length > 0) {
			List<Long> categoriesIdsList = Arrays.stream(categoriesIds).boxed().collect(Collectors.toList());
			List<Category> categories = categoryService.getCategoriesByIds(categoriesIdsList);
			categories = categories.stream().map(c -> {
				c.addMessage(message);
				return c;
			}).toList();
		}
		Message savedMessage = messageRepository.save(message);
		if (mailService.sendNewOfferRequestMail(fromUser, toUser, savedMessage))
			return savedMessage;
		else
			messageRepository.delete(savedMessage);
		return null;
	}

	@Override
	public void deleteMessageById(long id, long userId) throws BaseException {
		Optional<Message> message = messageRepository.findById(id);
		if (message.isEmpty()) {
			throw new MessageNotFoundException(id);
		} else if (message.get().getFromUser().getId() != userId) {
			throw new AccessDeniedException("User is not allowed to delete a message that was written by someone else!");
		} else {
			messageRepository.deleteById(id);
		}
	}

	@Override
	public Message getMessageById(long id, long userId) throws BaseException {
		Optional<Message> message = messageRepository.findById(id);
		if (message.isEmpty()) {
			throw new MessageNotFoundException(id);
		} else if (message.get().getFromUser().getId() != userId || message.get().getToUser().getId() != userId) {
			throw new AccessDeniedException("User is not allowed to delete a message that was written by someone else!");
		} else {
			return message.get();
		}
	}

	@Override
	public List<Message> getAllMessagesByFromUserId(long fromUserId) {
		return messageRepository.findAllByFromUserId(fromUserId);
	}

}
