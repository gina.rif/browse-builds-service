package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserExistsException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.services.UserService;
import com.mariarif.browsebuildsservice.domain.model.Invitation;
import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.infrastructure.security.TokenProvider;
import com.mariarif.browsebuildsservice.infrastructure.security.UserPrincipal;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private  TokenProvider tokenProvider;

	public UserServiceImpl(UserRepository userRepository, TokenProvider tokenProvider) {
		this.userRepository = userRepository;
		this.tokenProvider = tokenProvider;
	}

	public boolean existsUserWithEmail(String email) {
		Optional<User> user = userRepository.findByEmail(email);
		return user.isPresent();
	}
	
	public UserDetails loadUserByProviderId(String providerId) throws UsernameNotFoundException {
		try {
			return UserPrincipal.create(getUserByProviderId(providerId));
		} catch (BaseException ex) {
			throw new UsernameNotFoundException(String.format("User with google id %s not found!", providerId));
		}
	}

	public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {
		try {
			return UserPrincipal.create(getUserByEmail(email));
		} catch (BaseException ex) {
			throw new UsernameNotFoundException(String.format("User with email %s not found!", email));
		}
	}

	public User getUserById(long id) throws UserNotFoundException {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			return user.get();
		}
		
		if (log.isErrorEnabled()) {
			log.error("User with id " + id + " does not exist!");
		}
		throw new UserNotFoundException(id);
	}

	public User getUserByProviderId(String providerId) throws UserNotFoundException {
		Optional<User> user = userRepository.findByProviderId(providerId);
		if (user.isPresent()) {
			return user.get();
		}
		
		String msg = "User with google id " + providerId + " does not exist!";
		if (log.isErrorEnabled()) {
			log.error(msg);
		}
		throw new UserNotFoundException(msg);
	}

	public User getUserByEmail(String email) throws UserNotFoundException {
		Optional<User> user = userRepository.findByEmail(email);
		if (user.isPresent()) {
			return user.get();
		}
		
		String msg = "User with email " + email + " does not exist!";
		if (log.isErrorEnabled()) {
			log.error(msg);
		}
		throw new UserNotFoundException(msg);
	}

	@Override
	public User createUser(User user) throws BaseException {
		if(userRepository.findByEmail(user.getEmail()).isEmpty()) {
			String invitationToken = tokenProvider.createInvitationToken(user.getEmail());
			Invitation invitation = new Invitation(invitationToken,false, new Date());
			user.setInvitation(invitation);
			return userRepository.save(user);
		} else {
			if (log.isErrorEnabled()) {
				log.error("An user account with email " + user.getEmail() + " already exists!");
			}
			throw new UserExistsException(user.getEmail());
		}
	}
	
	@Override
	public User editUser(User user) throws BaseException {
		if(userRepository.existsById(user.getId())) {
			return userRepository.save(user);
		} else {
			if (log.isErrorEnabled()) {
				log.error("User with id " + user.getId() + " does not exist!");
			}
			throw new UserNotFoundException(user.getId());
		}
//		UserType userType = UserType.valueOf(user.getUserType());
//		if (userType.equals(UserType.COMPANY)) {
//			
//			return companyService.editCompany((Company) user);
//		} else if (user instanceof Client) {
//			return clientService.editClient((Client) user);
//		}
//		return null;
	}
	
	@Override
	public void deleteUserById(long id) throws BaseException {
		userRepository.deleteById(id);
	}

	@Override
	public User activateAccount(long id) throws UserNotFoundException {
		User user = getUserById(id);
		user.setEmailVerified(true);
		return editUser(user);
	}

	@Override
	public void deleteAccount(String providerId) throws BaseException {
		User user = getUserByProviderId(providerId);
		userRepository.delete(user);
	}

}
