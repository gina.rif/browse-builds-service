package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.PostNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.services.CategoryService;
import com.mariarif.browsebuildsservice.applicationservices.services.ClientService;
import com.mariarif.browsebuildsservice.applicationservices.services.CompanyService;
import com.mariarif.browsebuildsservice.applicationservices.services.PictureService;
import com.mariarif.browsebuildsservice.applicationservices.services.PostService;
import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.domain.model.Client;
import com.mariarif.browsebuildsservice.domain.model.Company;
import com.mariarif.browsebuildsservice.domain.model.Filter;
import com.mariarif.browsebuildsservice.domain.model.Picture;
import com.mariarif.browsebuildsservice.domain.model.Post;
import com.mariarif.browsebuildsservice.domain.model.SortType;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.PostRepository;

public class PostServiceImpl implements PostService {
	@Autowired
	private final PostRepository postRepository;

	@Autowired
	private final CategoryService categoryService;

	@Autowired
	private final CompanyService companyService;

	@Autowired
	private final ClientService clientService;

	@Autowired
	private final PictureService pictureService;

	public PostServiceImpl(PostRepository postRepository, CategoryService categoryService,
			CompanyService companyService, ClientService clientService, PictureService pictureService) {
		this.postRepository = postRepository;
		this.categoryService = categoryService;
		this.companyService = companyService;
		this.clientService = clientService;
		this.pictureService = pictureService;
	}

	@Override
	public Post createPost(Post post, long companyId, long[] categoriesIds,
			MultipartFile[] pictures) throws BaseException {
		Company company = companyService.getCompanyById(companyId);
		if (!company.getUser().getEmailVerified()) {
			throw new BaseException("Email not verified!");
		}
		company.addPost(post);

		if (categoriesIds.length > 0) {
			List<Long> categoriesIdsList = Arrays.stream(categoriesIds).boxed().collect(Collectors.toList());
			List<Category> categories = categoryService.getCategoriesByIds(categoriesIdsList);
			categories = categories.stream().map(c -> {
				c.addPost(post);
				return c;
			}).toList();
		}


		Picture picture;
		List<String> errorPictures = new ArrayList<>();
		for (MultipartFile pictureFile : pictures) {
			if (pictureFile != null)
				try {
					picture = pictureService.addPicture(pictureFile);
					post.addPicture(picture);
				} catch (IOException e) {
					errorPictures.add(pictureFile.getOriginalFilename());
				}
		} // TODO: handle error pictures
		return postRepository.save(post);
	}

	@Override
	public Post editPost(Post post) throws BaseException {
		if (postRepository.existsById(post.getId())) {
			return postRepository.save(post);
		} else
			throw new PostNotFoundException(post.getId());
	}

	@Override
	public void deletePostById(long id) throws BaseException {
		if (!postRepository.existsById(id)) {
			throw new PostNotFoundException(id);
		} else {
			postRepository.deleteById(id);
		}
	}

	@Override
	public Post getPostById(long id) throws BaseException {
		Post post = postRepository.findPostById(id).orElseThrow(() -> new PostNotFoundException(id));
		return post;
	}

	@Override
	public Page<Post> getAllPostsFilteredAndSorted(Filter filter, String sortString, int pageNo,
			Short elementsPerPage) {
		Sort sort = decideSort(sortString);
		Pageable pageRequest = PageRequest.of(pageNo, elementsPerPage, sort);
		Page<Post> allPostsFilteredAndSorted = postRepository.findAllByDatePostedInAndCompanyIdInAndCategoryIdIn(
				filter.getDateFrom(), 
				filter.getDateTo(),
				filter.getCompanyIds(), 
				filter.getCompanyIds().size(), 
				filter.getCategoryIds(), 
				filter.getCategoryIds().size(),
				filter.getAddressFilters().stream().map(a-> a.getCounty()).toList(),
				filter.getAddressFilters().stream().map(a-> a.getCity()).toList(),
				filter.getAddressFilters().size(),
				pageRequest);
		return allPostsFilteredAndSorted;
	}
	
	@Override
	public Page<Post> getAllPostsByCompanyId(long companyId, int pageNo,
			Short elementsPerPage) {
//		Sort sort = decideSort(sortString);
		Pageable pageRequest = PageRequest.of(pageNo, elementsPerPage, Sort.by(Sort.Direction.DESC, "date_posted"));
		Page<Post> posts = postRepository.findAllByCompanyId(companyId, pageRequest);
		return posts;
	}

	@Override
	public List<Post> getPostsLike(String text){
		return postRepository.findAllByDescriptionContainingIgnoreCase(text);
	}
	
	private Sort decideSort(String sortString) {
		SortType sortType = SortType.valueOf(sortString.toUpperCase());
		switch (sortType) {
		case DATE_POSTED:
			return Sort.by(Sort.Direction.ASC, "date_posted");
		case _DATE_POSTED:
			return Sort.by(Sort.Direction.DESC, "date_posted");
//		case _RATING:
//			return Sort.by(Sort.Direction.DESC, "rating")
		default:
			return Sort.by(Sort.Direction.DESC, "date_posted");
//			throw new IllegalArgumentException("Sort type /'" + sortString + "/' not supported.");
		}
	}

}
