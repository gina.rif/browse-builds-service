package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.CompanyNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserExistsException;
import com.mariarif.browsebuildsservice.applicationservices.services.CategoryService;
import com.mariarif.browsebuildsservice.applicationservices.services.CompanyService;
import com.mariarif.browsebuildsservice.applicationservices.services.MailService;
import com.mariarif.browsebuildsservice.applicationservices.services.PictureService;
import com.mariarif.browsebuildsservice.applicationservices.services.UserService;
import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.domain.model.Company;
import com.mariarif.browsebuildsservice.domain.model.Filter;
import com.mariarif.browsebuildsservice.domain.model.Picture;
import com.mariarif.browsebuildsservice.domain.model.SortType;
import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.CompanyRepository;

public class CompanyServiceImpl implements CompanyService {
	@Autowired
	private final CompanyRepository companyRepository;

	@Autowired
	private final UserService userService;

	@Autowired
	private final MailService mailService;

	@Autowired
	private final PictureService pictureService;

	@Autowired
	private final CategoryService categoryService;

	public CompanyServiceImpl(CompanyRepository companyRepository, UserService userService, MailService mailService,
			PictureService pictureService, CategoryService categoryService) {
		this.companyRepository = companyRepository;
		this.userService = userService;
		this.mailService = mailService;
		this.pictureService = pictureService;
		this.categoryService = categoryService;
	}

	@Override
	public Company createCompany(Company company, long[] categoryIds) throws BaseException {
		if (userService.existsUserWithEmail(company.getUser().getEmail())) {
			throw new UserExistsException(company.getUser().getEmail());
		}
		User user = userService.createUser(company.getUser());
		company.setUser(user);

		if (categoryIds.length > 0) {
			List<Long> categoryIdsList = Arrays.stream(categoryIds).boxed().collect(Collectors.toList());
			List<Category> categories = categoryService.getCategoriesByIds(categoryIdsList);
			categories = categories.stream().map(c -> {
				c.addCompany(company);
				return c;
			}).toList();
		}
		try {
			Company savedCompany = companyRepository.save(company);
			if (mailService.sendActivateAccountMail(savedCompany.getUser()))
				return savedCompany;
			else
				throw new Exception("Unable to send e-mail!");
		} catch(Exception e) {
			companyRepository.delete(company);
			userService.deleteUserById(user.getId());
			return null;
		}
		
	}

	@Override
	public Company editCompany(Company company, String phone, MultipartFile profilePicture, long[] categoryIds)
			throws BaseException, IOException {
		Optional<Company> oldCompany = companyRepository.findById(company.getId());
		if (oldCompany.isPresent()) {
			Company existentOldCompany = oldCompany.get();

			List<Long> oldCategoriesIdsList = existentOldCompany.getCategories().stream().map(c -> c.getId())
					.collect(Collectors.toList());
			List<Long> categoryIdsList = Arrays.stream(categoryIds).boxed().collect(Collectors.toList());

			List<Category> toRemoveCategories = existentOldCompany.getCategories().stream().filter(c -> !categoryIdsList.contains(c.getId())).toList();
			Iterator<Category> iterator = toRemoveCategories.iterator();
			while(iterator.hasNext()) {
				Category category = iterator.next();
				int index=existentOldCompany.getCategories().indexOf(category);
				existentOldCompany.getCategories().get(index).removeCompany(existentOldCompany);
			}
			
			List<Long> newCategoryIdsList = categoryIdsList.stream().filter(c -> !oldCategoriesIdsList.contains(c))
					.toList();

			List<Category> newCategories = categoryService.getCategoriesByIds(newCategoryIdsList);
			newCategories.forEach(c -> c.addCompany(existentOldCompany));

			if (profilePicture != null) {
				try {
					Picture picture = pictureService.addPicture(profilePicture);
					existentOldCompany.getUser().setImageUrl(picture.getPictureUrl());
				} catch (IOException e) {
					throw e;
				}
			}

			existentOldCompany.getUser().setPhone(phone);
			existentOldCompany.setCompanyName(company.getCompanyName());
			existentOldCompany.setDescription(company.getDescription());
			existentOldCompany.setAddress(company.getAddress());
			existentOldCompany.setTaxId(company.getTaxId());
			existentOldCompany.setRegistrationNumber(company.getRegistrationNumber());
			return companyRepository.save(existentOldCompany);
		} else
			throw new CompanyNotFoundException(company.getId());
	}

	@Override
	public void deleteCompanyById(long id) throws BaseException {
		if (!companyRepository.existsById(id)) {
			throw new CompanyNotFoundException(id);
		} else {
			companyRepository.deleteById(id);
		}
	}

	@Override
	public List<Company> getAllCompanies() throws BaseException {
		return companyRepository.findAll();
	}
	
	@Override
	public Page<Company> getAllCompaniesFilteredAndSorted(Filter filter, String sortString, int pageNo,
			Short elementsPerPage) throws BaseException {
		Sort sort = decideSort(sortString);
		Pageable pageRequest = PageRequest.of(pageNo, elementsPerPage, sort);
		Page<Company> allCompaniesFilteredAndSorted = companyRepository.findAllByCategoryIdInAndAddressIn(
				filter.getCategoryIds(), 
				filter.getCategoryIds().size(),
				filter.getAddressFilters().stream().map(a-> a.getCounty()).toList(),
				filter.getAddressFilters().stream().map(a-> a.getCity()).toList(),
				filter.getAddressFilters().size(),
				pageRequest);
		return allCompaniesFilteredAndSorted;
	}

	private Sort decideSort(String sortString) {
		SortType sortType = SortType.valueOf(sortString.toUpperCase());
		switch (sortType) {
		case ALPHABETICAL:
			return Sort.by(Sort.Direction.ASC, "company_name");
		case _ALPHABETICAL:
			return Sort.by(Sort.Direction.DESC, "company_name");
//		case _RATING:
//			return Sort.by(Sort.Direction.DESC, "rating")
		default:
			return Sort.by(Sort.Direction.ASC, "company_name");
		}
	}
	
	@Override
	public Company getCompanyById(long id) throws BaseException {
		Company company = companyRepository.findById(id).orElseThrow(() -> new CompanyNotFoundException(id));
		return company;
	}

	@Override
	public Company getCompanyByEmail(String email) throws BaseException {
		Company company = companyRepository.findByUserWithEmail(email)
				.orElseThrow(() -> new CompanyNotFoundException("Company with email " + email + " does not exist!"));
		return company;
	}

	@Override
	public List<Company> getCompaniesLike(String text) {
		return companyRepository.findAllByCompanyNameContainingIgnoreCase(text);
	}

}
