package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Scanner;

import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.mariarif.browsebuildsservice.applicationservices.services.MailService;
import com.mariarif.browsebuildsservice.domain.model.AuthProvider;
import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.domain.model.Message;
import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.domain.model.UserType;
import com.mariarif.browsebuildsservice.infrastructure.configuration.AppConfig;
import com.mariarif.browsebuildsservice.infrastructure.configuration.MailConfig;
import com.nimbusds.jose.util.StandardCharset;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MailServiceImpl implements MailService {
	private final AppConfig appConfig;

	private final MailConfig mailConfig;

	private final static String OFFER_REQUEST_MAIL_SUBJECT = "Cerere de lucrare pentru ";
	
	private final static String REPLY_SENDER_YAHOO = "https://compose.mail.yahoo.com/?to=${toUserEmail}&subject=${emailSubject}";
	
	private final static String REPLY_SENDER_GMAIL = "https://mail.google.com/mail/?view=cm&fs=1&to=${toUserEmail}&su=${emailSubject}";
	
	private final static String REPLY_RECEIVER_YAHOO = "https://compose.mail.yahoo.com/?to=${fromUserEmail}&subject=${emailSubject}";
	
	private final static String REPLY_RECEIVER_GMAIL = "https://mail.google.com/mail/?view=cm&fs=1&to=${fromUserEmail}&su=${emailSubject}";

	public MailServiceImpl(AppConfig appConfig, MailConfig mailConfig) {
		this.appConfig = appConfig;
		this.mailConfig = mailConfig;
	}

	@Override
	public boolean sendActivateAccountMail(User user) {
		try {
			String htmlTemplate = readFile("templateActivateAccountMail.html");
			htmlTemplate = htmlTemplate.replace("${clientUrl}", appConfig.getClientUrl());
			htmlTemplate = htmlTemplate.replace("${invitationToken}", user.getInvitation().getToken());
			String subject = "Activarea contului";
			sendMail(user, subject, htmlTemplate);
			return true;
		} catch (MessagingException | UnsupportedEncodingException e) {
			log.error("Mail was not sent!" + e.getMessage());
			return false;
		}
	}

	@Override
	public boolean sendNewOfferRequestMail(User fromUser, User toUser, Message message) {
		String categoriesWithLinks = formatCategoriesWithLinks(message.getCategories());
		boolean isSenderMailSent = sendNewOfferRequestSenderMail(fromUser, toUser, message, categoriesWithLinks);
		boolean isReceiverMailSent = sendNewOfferRequestReceiverMail(fromUser, toUser, message, categoriesWithLinks);
		return isSenderMailSent && isReceiverMailSent;
	}

	private boolean sendNewOfferRequestSenderMail(User fromUser, User toUser, Message message,
			String categoriesWithLinks) {
		try {
			String fromUserName = decideName(fromUser);
			String toUserName = decideName(toUser);
			String subject = OFFER_REQUEST_MAIL_SUBJECT + toUserName;
			String htmlTemplate = readFile("templateNewOfferRequestSenderMail.html");
			htmlTemplate = htmlTemplate.replace("${replySenderUrl}", AuthProvider.valueOf(toUser.getProvider()).equals(AuthProvider.google) 
					? REPLY_SENDER_GMAIL : REPLY_SENDER_YAHOO);
			htmlTemplate = htmlTemplate.replace("${clientUrl}", appConfig.getClientUrl());
			htmlTemplate = htmlTemplate.replace("${toUserId}", String.valueOf(toUser.getId()));
			htmlTemplate = htmlTemplate.replace("${toUserName}", toUserName);
			htmlTemplate = htmlTemplate.replace("${toUserEmail}", toUser.getEmail());
			htmlTemplate = htmlTemplate.replace("${fromUserId}", String.valueOf(fromUser.getId()));
			htmlTemplate = htmlTemplate.replace("${fromUserImageUrl}", fromUser.getImageUrl());
			htmlTemplate = htmlTemplate.replace("${fromUserName}", fromUserName);
			htmlTemplate = htmlTemplate.replace("${messageDate}", formatDate(message.getDate()));
			htmlTemplate = htmlTemplate.replace("${messageText}", message.getText());
			htmlTemplate = htmlTemplate.replace("${messageCounty}", message.getCounty());
			htmlTemplate = htmlTemplate.replace("${messageCity}", message.getCity());
			htmlTemplate = htmlTemplate.replace("${messageDateRequested}", formatDate(message.getDateRequested()));
			htmlTemplate = htmlTemplate.replace("${categoriesWithLinks}", categoriesWithLinks);
			htmlTemplate = htmlTemplate.replace("${emailSubject}", subject);
			sendMail(fromUser, subject, htmlTemplate);
			return true;
		} catch (MessagingException | UnsupportedEncodingException e) {
			log.error("Mail was not sent!" + e.getMessage());
			return false;
		}
	}
	
	private boolean sendNewOfferRequestReceiverMail(User fromUser, User toUser, Message message,
			String categoriesWithLinks) {
		try {
			String toUserName = decideName(toUser);
			String subject = OFFER_REQUEST_MAIL_SUBJECT + toUserName;
			String htmlTemplate = readFile("templateNewOfferRequestReceiverMail.html");
			htmlTemplate = htmlTemplate.replace("${replyReceiverUrl}", AuthProvider.valueOf(toUser.getProvider()).equals(AuthProvider.google) 
					? REPLY_RECEIVER_GMAIL : REPLY_RECEIVER_YAHOO);
			htmlTemplate = htmlTemplate.replace("${clientUrl}", appConfig.getClientUrl());
			htmlTemplate = htmlTemplate.replace("${fromUserId}", String.valueOf(fromUser.getId()));
			htmlTemplate = htmlTemplate.replace("${fromUserImageUrl}", fromUser.getImageUrl());
			htmlTemplate = htmlTemplate.replace("${fromUserEmail}", fromUser.getEmail());
			htmlTemplate = htmlTemplate.replace("${fromUserName}", decideName(fromUser));
			htmlTemplate = htmlTemplate.replace("${messageDate}", formatDate(message.getDate()));
			htmlTemplate = htmlTemplate.replace("${messageText}", message.getText());
			htmlTemplate = htmlTemplate.replace("${messageCounty}", message.getCounty());
			htmlTemplate = htmlTemplate.replace("${messageCity}", message.getCity());
			htmlTemplate = htmlTemplate.replace("${messageDateRequested}", formatDate(message.getDateRequested()));
			htmlTemplate = htmlTemplate.replace("${categoriesWithLinks}", categoriesWithLinks);
			htmlTemplate = htmlTemplate.replace("${emailSubject}", subject);
			
			sendMail(toUser, subject, htmlTemplate);
			return true;
		} catch (MessagingException | UnsupportedEncodingException e) {
			log.error("Mail was not sent!" + e.getMessage());
			return false;
		}
	}

	private String decideName(User user) {
		if (UserType.valueOf(user.getUserType()) == UserType.COMPANY) {
			return user.getCompany().getCompanyName();
		} else if (UserType.valueOf(user.getUserType()) == UserType.CLIENT) {
			return user.getClient().getFirstName() + " " + user.getClient().getLastName();
		} else
			return "";
	}

	private String formatDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", new Locale("ro", "RO"));
		String formattedDate = dateFormat.format(date);
		return formattedDate;

	}

	private void sendMail(User user, String subject, String content)
			throws MessagingException, UnsupportedEncodingException {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		mailSender.setHost(mailConfig.getHost());
		mailSender.setPort(mailConfig.getPort());
		mailSender.setUsername(mailConfig.getUsername());
		mailSender.setPassword(mailConfig.getPassword());

		Properties mailProperties = new Properties();
		mailProperties.setProperty("mail.smtp.auth", mailConfig.getPropertiesMailSmtpAuth());
		mailProperties.setProperty("mail.smtp.starttls.enable", mailConfig.getPropertiesMailSmtpStarttlsEnable());

		mailSender.setJavaMailProperties(mailProperties);

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");

		helper.setFrom(mailConfig.getUsername(), "Browse Builds Contact");
		helper.setTo(user.getEmail());
		helper.setSubject(subject);

		helper.setText(content, true);

		helper.addInline("green-glow1.png", new ClassPathResource("images/green-glow1.png"));
		helper.addInline("top-rounded.png", new ClassPathResource("images/top-rounded.png"));
		helper.addInline("divider.png", new ClassPathResource("images/divider.png"));
		helper.addInline("facebook2x.png", new ClassPathResource("images/facebook2x.png"));
		helper.addInline("twitter2x.png", new ClassPathResource("images/twitter2x.png"));
		helper.addInline("instagram2x.png", new ClassPathResource("images/instagram2x.png"));
		helper.addInline("bottom-rounded.png", new ClassPathResource("images/bottom-rounded.png"));
		helper.addInline("buildings-bg.jpg", new ClassPathResource("images/buildings-bg.jpg"));
		helper.addInline("logoSimple.png", new ClassPathResource("images/logoSimple.png"));

		mailSender.send(message);
		log.debug("Mail sent to " + user.getEmail());
	}

	private String readFile(String filepath) {

		try (Scanner scanner = new Scanner(new ClassPathResource(filepath).getInputStream(),
				StandardCharset.UTF_8.name())) {
			scanner.useDelimiter("\\A");
			return scanner.hasNext() ? scanner.next() : "";
		} catch (IOException e) {
			log.error("File " + filepath + " could not be read" + e);
			return "";
		}
	}

	private String formatCategoriesWithLinks(List<Category> categories) {
		StringBuilder categoryLinks = new StringBuilder();
		for (Category category : categories) {
			String categoryLink = "<a href=\"" + appConfig.getClientUrl() + "/?filter={\"categories\"=[" + category.getId()
					+ "]}\">" + category.getName() + "</a>";
			categoryLinks.append(categoryLink).append(", ");
		}
		String categoriesWithLinks = categoryLinks.toString();
		if (categoriesWithLinks.endsWith(", ")) {
			categoriesWithLinks = categoriesWithLinks.substring(0, categoriesWithLinks.length() - 2);
			// Remove the trailing comma and space
		}
		return categoriesWithLinks;
	}
}
