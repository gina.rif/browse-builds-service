package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.ClientNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserExistsException;
import com.mariarif.browsebuildsservice.applicationservices.services.ClientService;
import com.mariarif.browsebuildsservice.applicationservices.services.MailService;
import com.mariarif.browsebuildsservice.applicationservices.services.PictureService;
import com.mariarif.browsebuildsservice.applicationservices.services.UserService;
import com.mariarif.browsebuildsservice.domain.model.Client;
import com.mariarif.browsebuildsservice.domain.model.Picture;
import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.ClientRepository;

public class ClientServiceImpl implements ClientService {
	@Autowired
	private final ClientRepository clientRepository;
	
	@Autowired
	private final UserService userService;
	
	@Autowired
	private final MailService mailService;
	
	@Autowired
	private final PictureService pictureService;

	public ClientServiceImpl(ClientRepository clientRepository,  UserService userService, MailService mailService, PictureService pictureService) {
		this.clientRepository = clientRepository;
		this.userService = userService;
		this.mailService = mailService;
		this.pictureService = pictureService;
	}

	@Override
	public Client createClient(Client client) throws BaseException {
		if(userService.existsUserWithEmail(client.getUser().getEmail())) {
			throw new UserExistsException(client.getUser().getEmail());
		}
		
		User user=userService.createUser(client.getUser());
		client.setUser(user);
		Client savedClient = clientRepository.save(client);
		if (mailService.sendActivateAccountMail(savedClient.getUser()))
			return savedClient;
		else
			
			clientRepository.delete(savedClient);
			userService.deleteUserById(savedClient.getId());
		return null;
	}

	@Override
	public Client editClient(Client client, String phone, MultipartFile profilePicture)
			throws BaseException, IOException {
		Optional<Client> oldClient = clientRepository.findById(client.getId());
		if (oldClient.isPresent()) {
			Client existentOldClient = oldClient.get();

			if (profilePicture != null) {
				try {
					Picture picture = pictureService.addPicture(profilePicture);
					existentOldClient.getUser().setImageUrl(picture.getPictureUrl());
				} catch (IOException e) {
					throw e;
				}
			}

			existentOldClient.getUser().setPhone(phone);
			existentOldClient.setFirstName(client.getFirstName());
			existentOldClient.setLastName(client.getLastName());
			return clientRepository.save(existentOldClient);
		} else
			throw new ClientNotFoundException(client.getId());
	}

	@Override
	public void deleteClientById(long id) throws BaseException {
		if (!clientRepository.existsById(id)) {
			throw new ClientNotFoundException(id);
		} else {
			clientRepository.deleteById(id);
		}
	}

	@Override
	public Client getClientById(long id) throws BaseException {
		Client client = clientRepository.findById(id).orElseThrow(() -> new ClientNotFoundException(id));
		return client;
	}
	
	@Override
	public List<Client> getClientsLike(String text){
		return clientRepository.findAllByFullNameContainingIgnoreCase(text);
	}
}
