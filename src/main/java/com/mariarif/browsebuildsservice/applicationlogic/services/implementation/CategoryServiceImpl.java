package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.CategoryExistsException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.CategoryNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.services.CategoryService;
import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.CategoryRepository;

public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private final CategoryRepository categoryRepository;

	public CategoryServiceImpl(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	@Override
	public Category createCategory(Category category) throws BaseException {
		if (categoryRepository.existsById(category.getId())) {
			throw new CategoryExistsException(category.getId());
		}
		return categoryRepository.save(category);
	}

	@Override
	public Category editCategory(Category category) throws BaseException {
		if (categoryRepository.existsById(category.getId())) {
			return categoryRepository.save(category);
		} else
			throw new CategoryNotFoundException(category.getId());
	}

	@Override
	public void deleteCategoryById(long id) throws BaseException {
		if (!categoryRepository.existsById(id)) {
			throw new CategoryNotFoundException(id);
		} else {
			categoryRepository.deleteById(id);
		}
	}

	@Override
	public Category getCategoryById(long id) throws BaseException {
		Category category = categoryRepository.findById(id).orElseThrow(() -> new CategoryNotFoundException(id));
		return category;
	}

	@Override
	public List<Category> getAllCategories() {
		return categoryRepository.findAll();
	}

	@Override
	public List<Category> getCategoriesByIds(List<Long> ids) throws BaseException {
		return categoryRepository.findAllById(ids);
	}

	@Override
	public List<Category> getCategoriesLike(String text){
		return categoryRepository.findAllByNameContainingIgnoreCase(text);
	}
	
	private List<Long> convertPrimitiveArrayToList(long[] primitiveArray) {
		List<Long> list = new ArrayList<>();
		if (primitiveArray.length > 0) {
			for (long value : primitiveArray) {
				list.add(value);
			}
		}
		return list;
	}
}
