package com.mariarif.browsebuildsservice.applicationlogic.services.implementation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.PictureNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.services.FileService;
import com.mariarif.browsebuildsservice.applicationservices.services.PictureService;
import com.mariarif.browsebuildsservice.domain.model.Picture;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.PictureRepository;

import io.micrometer.common.lang.NonNull;

public class PictureServiceImpl implements PictureService {
	private final String POSTS_CONTAINER = "posts-container";

	@Autowired
	private final PictureRepository pictureRepository;

	@Autowired
	private final FileService fileService;

	public PictureServiceImpl(PictureRepository pictureRepository, FileService fileService) {
		this.pictureRepository = pictureRepository;
		this.fileService = fileService;
	}

	@Override
	public Picture addPicture(@NonNull MultipartFile file) throws IOException {
		String pictureUrl = fileService.uploadFile(file, POSTS_CONTAINER);
		Picture picture = new Picture(pictureUrl);
		return pictureRepository.save(picture);
	}

	@Override
	public void deletePictureById(long id) throws BaseException {
		if (!pictureRepository.existsById(id)) {
			throw new PictureNotFoundException(id);
		} else {
			pictureRepository.deleteById(id);
		}
	}

	@Override
	public Picture getPictureById(long id) throws BaseException {
		Picture picture = pictureRepository.findById(id).orElseThrow(() -> new PictureNotFoundException(id));
		return picture;
	}

//	@Override
//	public List<Picture> getAllPicturesByPostId(long postId) {
//		return pictureRepository.findByPostId(postId);
//	}
}
