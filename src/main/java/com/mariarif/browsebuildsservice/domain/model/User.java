package com.mariarif.browsebuildsservice.domain.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "USERS")
@NoArgsConstructor
public class User extends Object {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "provider_id")
	private String providerId;
	
	@Column(name = "provider", length = 10, nullable = false)
	@Size(max = 10)
	private String provider;

	@Column(name = "email", unique = true, length = 30, nullable = false)
	@Size(max = 30)
	private String email;

	@Column(name = "phone", length = 12, nullable = false)
	@Size(max = 12)
	private String phone;

	@Column(name = "image_url")
	private String imageUrl;

	@Column(name = "email_verified", nullable = false)
	private Boolean emailVerified = false;

	@Column(name = "user_type", length = 10, nullable = false)
	@Size(max = 10)
	private String userType;

	@JsonIgnore
	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = true)
	private Company company;

	@JsonIgnore
	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = true)
	private Client client;

	@JsonIgnore
	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Invitation invitation;

	@JsonIgnore
	@OneToMany(mappedBy = "fromUser", cascade ={ CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<Message> messagesSent = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "toUser", cascade ={ CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<Message> messagesReceived = new ArrayList<>();

	public User(long id, String providerId, String provider, String email, String phone, String imageUrl,
			Boolean emailVerified, String userType, Company company, Client client, Invitation invitation,
			List<Message> messagesSent, List<Message> messagesReceived) {
		this.id = id;
		this.providerId = providerId;
		this.provider = provider;
		this.email = email;
		this.phone = phone;
		this.imageUrl = imageUrl;
		this.emailVerified = emailVerified;
		this.userType = userType;
		this.company = company;
		this.client = client;
		this.invitation = invitation;
		if (messagesSent == null) {
			messagesSent = new ArrayList<>();
		}
		this.messagesSent = messagesSent;
		if (messagesReceived == null) {
			messagesReceived = new ArrayList<>();
		}
		this.messagesReceived = messagesReceived;
	}

	public void setClient(Client client) {
		if (client != null) {
			this.client = client;
			client.setUser(this);
		}
	}

	public void setCompany(Company company) {
		if (company != null) {
			this.company = company;
			company.setUser(this);
		}
	}

	public void setInvitation(Invitation invitation) {
		if (invitation != null) {
			this.invitation = invitation;
			invitation.setUser(this);
		}
	}

	public void addMessageSent(Message message) {
		if (message != null) {
			this.messagesSent.add(message);
			message.setFromUser(this);
		}
	}

	public void removeMessageSent(Message message) {
		if (message != null) {
			message.setFromUser(null);
			this.messagesSent.remove(message);
		}
	}

	public void addMessageReceived(Message message) {
		if (message != null) {
			this.messagesReceived.add(message);
			message.setToUser(this);
		}
	}

	public void removeMessageReceived(Message message) {
		if (message != null) {
			message.setToUser(null);
			this.messagesReceived.remove(message);
		}
	}

}
