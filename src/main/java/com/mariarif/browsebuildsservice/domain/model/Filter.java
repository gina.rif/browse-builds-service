package com.mariarif.browsebuildsservice.domain.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Component
@Data
@Getter
public class Filter implements Serializable {

	private static final long serialVersionUID = -1513227899281111348L;

	String dateFrom;

	String dateTo;

	List<Long> companyIds;

	List<Long> categoryIds;

	List<Short> ratingAverages;

	List<AddressFilter> addressFilters;

	public Filter() {
		dateFrom = "";
		dateTo = "";
		companyIds = new ArrayList<>();
		categoryIds = new ArrayList<>();
		ratingAverages = new ArrayList<>();
		addressFilters = new ArrayList<>();
	}

	public Filter(String dateFrom, String dateTo, List<Long> companyIds, List<Long> categoryIds,
			List<AddressFilter> addressFilters, List<Short> ratingAverages) {
		this.dateFrom = dateFrom != null ? dateFrom : "";
		this.dateTo = dateTo != null ? dateTo + " 23:59:59.999999" : "";
		if (companyIds != null) {
			this.companyIds = companyIds;
		} else {
			this.companyIds = new ArrayList<>();
		}
		if (categoryIds != null) {
			this.categoryIds = categoryIds;
		} else {
			this.categoryIds = new ArrayList<>();
		}
		if (addressFilters != null) {
			this.addressFilters = addressFilters;
		} else {
			this.addressFilters = new ArrayList<>();
		}
		if (ratingAverages != null) {
			this.ratingAverages = ratingAverages;
		} else {
			this.ratingAverages = new ArrayList<>();
		}
	}

	public void setDateFrom(String dateFrom) {
		if (dateFrom != null) {
			this.dateFrom = dateFrom;
		}
	}

	public void setDateTo(String dateTo) {
		if (dateTo != null) {
			this.dateTo = dateTo;
		}
	}

	public void setCompanyIds(List<Long> companyIds) {
		if (companyIds != null) {
			this.companyIds = companyIds;
		}
	}

	public void setCategoryIds(List<Long> categoryIds) {
		if (categoryIds != null) {
			this.categoryIds = categoryIds;
		}
	}

	public void setAddressFilters(List<AddressFilter> addressFilters) {
		if (addressFilters != null) {
			this.addressFilters = addressFilters;
		}
	}

	public void setRatingAverages(List<Short> ratingAverages) {
		if (ratingAverages != null) {
			this.ratingAverages = ratingAverages;
		}
	}
}
