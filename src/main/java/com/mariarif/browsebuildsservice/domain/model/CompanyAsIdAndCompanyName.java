package com.mariarif.browsebuildsservice.domain.model;

public interface CompanyAsIdAndCompanyName {
	long getId();

	String getCompanyName();
}
