package com.mariarif.browsebuildsservice.domain.model;

public enum UserType {
	COMPANY, CLIENT
}
