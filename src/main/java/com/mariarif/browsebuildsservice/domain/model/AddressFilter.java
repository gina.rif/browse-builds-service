package com.mariarif.browsebuildsservice.domain.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public record AddressFilter(String county, String city) {
	public static List<AddressFilter> parseAddressFilters(JSONArray jsonArray) throws JSONException {
		List<AddressFilter> addressFilters = new ArrayList<>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String county = jsonObject.getString("county");
				String city = jsonObject.getString("city");
				AddressFilter addressFilter = new AddressFilter(county, city);
				addressFilters.add(addressFilter);
			}
		}
		return addressFilters;
	}

	public String getCounty() {
		return county;
	}

	public String getCity() {
		return city;
	}
}
