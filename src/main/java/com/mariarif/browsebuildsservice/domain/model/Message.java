package com.mariarif.browsebuildsservice.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Component
@Entity
@Data
@Table(name = "MESSAGES")
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "from_user_id", nullable = false)
	private User fromUser;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "to_user_id", nullable = false)
	private User toUser;

	@Column(name = "text", columnDefinition = "nvarchar(4000)", nullable = false)
	@Size(max = 4000)
	private String text;

	@Column(name = "county", columnDefinition = "nvarchar(20)", nullable = false)
	@Size(max = 20)
	private String county;

	@Column(name = "city", columnDefinition = "nvarchar(50)", nullable = false)
	@Size(max = 50)
	private String city;

	@ManyToMany(/* fetch = FetchType.LAZY, */ cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinTable(name = "messages_categories", joinColumns = @JoinColumn(name = "message_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
	private List<Category> categories = new ArrayList<>();

	@Column(name = "date_requested", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dateRequested;

	@Column(name = "date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	public Message() {
		categories = new ArrayList<>();
	}

	public Message(long id, User fromUser, User toUser, @Size(max = 5000) String text, String county,String city,List<Category> categories,
			Date dateRequested, Date date) {
		this.id = id;
		this.fromUser = fromUser;
		this.toUser = toUser;
		this.text = text;
		this.county = county;
		this.city = city;
		if (categories == null) {
			categories = new ArrayList<>();
		}
		this.categories = categories;
		this.dateRequested = dateRequested;
		this.date = date;
	}

	public void addCategory(Category category) {
		if (category != null) {
			this.categories.add(category);
		}
	}

	public void removeCategory(Category category) {
		if (category != null) {
			this.categories.remove(category);
		}
	}
}
