package com.mariarif.browsebuildsservice.domain.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "CATEGORIES")
@NoArgsConstructor
public class Category implements CategoryAsIdAndNameAndParentCategory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name", columnDefinition = "nvarchar(100)", nullable = false)
	@Size(max = 30)
	private String name;

//	@Column(name = "subcategories")
	@OneToMany(mappedBy = "parentCategory", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<Category> subcategories = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	private Category parentCategory;

	@ManyToMany(mappedBy = "categories", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<Post> posts = new ArrayList<>();

	@ManyToMany(mappedBy = "categories", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<Company> companies = new ArrayList<>();

	@ManyToMany(mappedBy = "categories", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<Message> messages = new ArrayList<>();
	
	public Category(long id) {
		this.id = id;
		this.name = null;
		this.subcategories = new ArrayList<>();
		this.parentCategory = null;
	}

	public Category(long id, @Size(max = 30) String name, List<Category> subcategories, Category parentCategory,
			List<Post> posts, List<Company> companies) {
		this.id = id;
		this.name = name;
		if (subcategories == null) {
			subcategories = new ArrayList<>();
		}
		this.subcategories = subcategories;
		this.parentCategory = parentCategory;
		if (posts == null) {
			posts = new ArrayList<>();
		}
		this.posts = posts;
		if (companies == null) {
			companies = new ArrayList<>();
		}
		this.companies = companies;
	}

	public void addPost(Post post) {
		if (post != null) {
			this.posts.add(post);
			post.getCategories().add(this);
		}
	}

	public void removePost(Post post) {
		if (post != null) {
			this.posts.remove(post);
			post.getCategories().remove(this);
		}
	}

	public void addCompany(Company company) {
		if (company != null) {
			this.companies.add(company);
			company.addCategory(this);
		}
	}

	public void removeCompany(Company company) {
		if (company != null) {
			this.companies.remove(company);
			company.removeCategory(this);
		}
	}

	public void addSubcategory(Category category) {
		if (category != null) {
			this.subcategories.add(category);
			category.setParentCategory(this);
		}
	}

	public void removeSubcategory(Category category) {
		if (category != null) {
			category.setParentCategory(null);
			this.subcategories.remove(category);
		}
	}
	
	public void addMessage(Message message) {
		if (message != null) {
			this.messages.add(message);
			message.addCategory(this);}
	}

	public void removeMessage(Message message) {
		if (message != null) {
			message.removeCategory(this);
			this.messages.remove(message);
		}
	}
//	public Category(@Size(max = 30) String name, Map<Long, Category> subcategories, Category parentCategory) {
//		this.name = name;
//		if (subcategories == null) {
//			subcategories = new ArrayList<>();
//		}
//		this.subcategories = subcategories;
//		this.parentCategory = parentCategory;
//	}

//	public Category() {
//		name = null;
//		subcategories = new ArrayList<>();
//		parentCategory = null;
//	}

}
