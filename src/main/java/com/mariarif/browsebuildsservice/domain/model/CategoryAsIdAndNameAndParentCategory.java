package com.mariarif.browsebuildsservice.domain.model;

public interface CategoryAsIdAndNameAndParentCategory {
	long getId();

	String getName();

	Category getParentCategory();
}
