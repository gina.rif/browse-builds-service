package com.mariarif.browsebuildsservice.domain.model;

import java.util.Date;

import org.springframework.stereotype.Component;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "INVITATIONS")
@NoArgsConstructor
public class Invitation {
	@Id
	private long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(unique = true, length = 400)
	@Size(max = 400)
	private String token;

	@Column
	private Boolean used;

	@Column(name = "generated_at")
	private Date geteratedAt;

	public Invitation(String token, Boolean used, Date generatedAt) {
		this.token = token;
		this.used = used;
		this.geteratedAt = generatedAt;
	}
}
