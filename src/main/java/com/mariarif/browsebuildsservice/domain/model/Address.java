package com.mariarif.browsebuildsservice.domain.model;

import org.springframework.stereotype.Component;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "ADDRESSES")
@NoArgsConstructor
public class Address {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id;
		
		@MapsId
		@OneToOne(fetch = FetchType.LAZY)
		private Company company;
		
		@Column(name="county", columnDefinition = "nvarchar(50)")
		private String county;
		
		@Column(columnDefinition = "nvarchar(50)")
		private String city;
		
		@Column(columnDefinition = "nvarchar(50)")
		private String street;
		
		@Column
		private String number;
		
		@Column
		private double latitude;
		
		@Column
		private double longitude;

		public Address(long id, Company company, String county, String city, String street, String number, double latitude, double longitude) {
			this.id = id;
			this.company = company;
			this.county = county;
			this.city = city;
			this.street = street;
			this.number = number;
			this.latitude = latitude;
			this.longitude=longitude;
		}

		public Address(String county, String city, String street, String number, double latitude, double longitude) {
			this.county = county;
			this.city = city;
			this.street = street;
			this.number = number;
			this.latitude = latitude;
			this.longitude=longitude;
		}
}