package com.mariarif.browsebuildsservice.domain.model;

import org.springframework.stereotype.Component;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "PICTURES")
@NoArgsConstructor
public class Picture {


		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id;

//		@ManyToOne(fetch = FetchType.LAZY)
//		@JoinColumn(name = "post_id", nullable = false)
//		private Post post;
		
		@Column
		private String pictureUrl;

		public Picture(long id, String pictureUrl) {
			this.id = id;
			this.pictureUrl = pictureUrl;
		}

		public Picture(String pictureUrl) {
			this.pictureUrl = pictureUrl;
		}
		

}