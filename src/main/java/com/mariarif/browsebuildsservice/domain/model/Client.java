package com.mariarif.browsebuildsservice.domain.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "CLIENTS")
@NoArgsConstructor
public class Client{

	@Id
	private long id;

	@JsonIgnore
	@OneToOne//(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(name = "first_name", columnDefinition = "nvarchar(50)", nullable = false)
	private String firstName;

	@Column(name = "last_name", columnDefinition = "nvarchar(50)", nullable = false)
	private String lastName;

	public Client(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Client(long id, User user, String firstName, String lastName) {
		this.id = id;
		this.user = user;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public void setUser(User user) {
		if (user != null) {
			this.user = user;
		}
	}

}