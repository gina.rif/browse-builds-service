package com.mariarif.browsebuildsservice.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "POSTS")
@NoArgsConstructor
public class Post {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(/*fetch = FetchType.LAZY,*/ optional = false)
	@JoinColumn(name = "company_id", nullable = false)
	private Company company;

	@Column(name = "description", columnDefinition = "nvarchar(3000)", nullable = false)
	@Size(max = 3000)
	private String description;

	@ManyToMany(/* fetch = FetchType.LAZY, */ cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinTable(name = "posts_categories", joinColumns = @JoinColumn(name = "post_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
	private List<Category> categories = new ArrayList<>();


	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<Picture> pictures = new ArrayList<>();

	@Column(name = "date_posted", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date datePosted;

	public Post(String description, Date datePosted) {
		this.description = description;
		this.datePosted = datePosted;
	}

	public Post(long id, Company company, String description, List<Category> categories,
			List<Picture> pictures, Date datePosted) {
		this.id = id;
		this.company = company;
		this.description = description;
		if (categories == null) {
			categories = new ArrayList<>();
		}
		this.categories = categories;
		if (pictures == null) {
			pictures = new ArrayList<>();
		}
		this.pictures = pictures;
		this.datePosted = datePosted;
	}
	
	public void addPicture(Picture picture) {
		if(picture!=null) {
			this.pictures.add(picture);
		}
	}
	
	public void removePicture(Picture picture) {
		if(picture!=null) {
			this.pictures.remove(picture);
		}
	}
}
