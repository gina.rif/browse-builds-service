package com.mariarif.browsebuildsservice.domain.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mariarif.browsebuildsservice.domain.annotation.ValidRegistrationNumber;
import com.mariarif.browsebuildsservice.domain.annotation.ValidTaxId;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "COMPANIES")
@NoArgsConstructor
public class Company implements CompanyAsIdAndCompanyName{

	@Id
	private long id;

	@JsonIgnore
	@OneToOne//(fetch = FetchType.LAZY)
	@MapsId
	private User user;

	@Column(name = "company_name", columnDefinition = "nvarchar(100)", nullable = false)
	private String companyName;

	@Column(name = "description",columnDefinition = "nvarchar(900)")
	@Size(max = 900)
	private String description;
	
	@OneToOne(mappedBy = "company", cascade = CascadeType.ALL,
            fetch = FetchType.EAGER, optional = false)
	private Address address;
	
	@ValidTaxId
	@Column(name = "tax_id", nullable = false)
	private String taxId; // Legal Tax ID for the company
	
	@ValidRegistrationNumber
	@Column(name = "registration_number", nullable = false)
	private String registrationNumber; // Registration number for the company

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "companies_categories", joinColumns = @JoinColumn(name = "company_id", referencedColumnName = "user_id"), inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
	private List<Category> categories = new ArrayList<>();
	
	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<Post> posts;

	public Company(long id) {
		this.id = id;
		this.user = new User();
		this.companyName = null;
		this.description = null;
		this.address = null;
		this.taxId = null;
		this.registrationNumber = null;
		this.categories = new ArrayList<>();
		this.posts = new ArrayList<>();
	}

	public Company(long id, User user, String companyName, String description, Address address, String taxId,
			String registrationNumber, List<Category>categories, List<Post> posts) {
		this.id = id;
		if (user == null) {
			user = new User();
		}
		this.user = user;
		this.companyName = companyName;
		this.description = description;
		this.address = address;
		this.taxId = taxId;
		this.registrationNumber = registrationNumber;
		if (categories == null) {
			categories = new ArrayList<>();
		}
		this.categories =categories;
		if (posts == null) {
			posts = new ArrayList<>();
		}
		this.posts = posts;

	}
	
	public void setAddress(Address address) {
		if (address != null) {
			address.setCompany(this);
			this.address = address;
		}
	}
	
	public void setUser(User user) {
		if (user != null) {
			this.user = user;
		}
	}
	
	public void addPost(Post post) {
		if(post != null) {
			this.posts.add(post);
			post.setCompany(this);
		}
	}
	
	public void removePost(Post post) {
		if(post != null) {
			post.setCompany(null);
			this.posts.remove(post);
		}
	}
	
	public void addCategory(Category category) {
		if(category != null) {
			this.categories.add(category);
		}
	}
	
	public void removeCategory(Category category) {
		if(category != null) {
			this.categories.remove(category);
		}
	}
}
