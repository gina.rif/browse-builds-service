package com.mariarif.browsebuildsservice.domain.annotation;

import java.util.regex.Pattern;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class TaxIdValidator implements ConstraintValidator<ValidTaxId, String> {

	private static final Pattern CIF_PATTERN = Pattern.compile("^(RO)?[1-9]\\d{1,9}$");
	private static final short[] REVERSED_TEST_KEY = { 2, 3, 5, 7, 1, 2, 3, 5, 7 };

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null || value.isBlank()) {
			return true;
		}
		if (CIF_PATTERN.matcher(value).matches()) {
			char controlChar = value.contains("RO") ? value.charAt(11) : value.charAt(9);

			String code = (value.contains("RO") ? value.substring(2, 11) : value.substring(0, 9));
			int sum = 0;
			for (int i = 0; i < 9; i++) {
				sum += REVERSED_TEST_KEY[i] * Short.valueOf(code.substring(9 - 1 - i, 9 - i));
			}
			char verificationChar = String.valueOf(sum * 10 % 11 % 10).charAt(0);
			if (verificationChar == controlChar) {
				return true;
			}
		}
		return false;
	}

}
