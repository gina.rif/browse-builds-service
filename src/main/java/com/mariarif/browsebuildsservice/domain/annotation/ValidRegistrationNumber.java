package com.mariarif.browsebuildsservice.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RegistrationNumberValidator.class)
public @interface ValidRegistrationNumber {
	String message() default "Invalid registration number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
