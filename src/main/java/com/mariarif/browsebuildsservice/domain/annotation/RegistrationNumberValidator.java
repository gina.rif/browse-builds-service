package com.mariarif.browsebuildsservice.domain.annotation;

import java.util.regex.Pattern;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class RegistrationNumberValidator implements ConstraintValidator<ValidRegistrationNumber, String> {

	private static final Pattern REG_NUMBER_PATTERN = Pattern.compile("^(J|F|C)((0[1-9])|([1-3][0-9])|(40|51|52))\\/([1-9][0-9]{0,7})\\/[1-9][0-9]{3}$");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.isBlank()) {
            return true;
        }
        return REG_NUMBER_PATTERN.matcher(value).matches();
    }

}
