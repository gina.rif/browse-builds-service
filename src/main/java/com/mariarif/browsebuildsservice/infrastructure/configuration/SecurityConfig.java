package com.mariarif.browsebuildsservice.infrastructure.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.mariarif.browsebuildsservice.infrastructure.security.RestAuthenticationEntryPoint;
import com.mariarif.browsebuildsservice.infrastructure.security.TokenAuthenticationFilter;
import com.mariarif.browsebuildsservice.infrastructure.security.oauth2.CustomOAuth2UserService;
import com.mariarif.browsebuildsservice.infrastructure.security.oauth2.HttpCookieOAuth2AuthorizationRequestRepository;
import com.mariarif.browsebuildsservice.infrastructure.security.oauth2.OAuth2AuthenticationFailureHandler;
import com.mariarif.browsebuildsservice.infrastructure.security.oauth2.OAuth2AuthenticationSuccessHandler;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig {
	@Autowired
	private CustomOAuth2UserService customOAuth2UserService;
	@Autowired
	private OAuth2AuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler;
	@Autowired
	private OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;
	@Autowired
	private HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository;

	@Bean
	public TokenAuthenticationFilter tokenAuthenticationFilter() {
		return new TokenAuthenticationFilter();
	}

	/*
	 * By default, Spring OAuth2 uses
	 * HttpSessionOAuth2AuthorizationRequestRepository to save the authorization
	 * request. But, since our service is stateless, we can't save it in the
	 * session. We'll save the request in a Base64 encoded cookie instead.
	 */
	@Bean
	public HttpCookieOAuth2AuthorizationRequestRepository cookieAuthorizationRequestRepository() {
		return new HttpCookieOAuth2AuthorizationRequestRepository();
	}

	@SuppressWarnings("unchecked")
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.cors()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.csrf().disable()
			.formLogin().disable()
			.httpBasic().disable()
			.exceptionHandling().authenticationEntryPoint(new RestAuthenticationEntryPoint())
			.and().authorizeHttpRequests()
				.requestMatchers(
						HttpMethod.GET, 
						"/categories", "/categories/*",
						"/companies", "/companies/v1", "/companies/v2",
						"/invitations", "/invitations/activate",
						"/posts", "/posts/*", "/posts/company/*",
						"/search",
						"/users/{id}")
				.permitAll()
				.requestMatchers(
						HttpMethod.POST, 
						"/clients", 
						"/companies")
				.permitAll()
				.requestMatchers("/error", "/swagger-ui", "/auth/**", "/oauth2/authorize", "/oauth2/callback/*")
				.permitAll()
				.anyRequest()
				.authenticated()
				.and()
				.oauth2Login()
				.authorizationEndpoint()
				.baseUri("/oauth2/authorize")
				.authorizationRequestRepository(cookieAuthorizationRequestRepository())
				.and().
				redirectionEndpoint()
				.baseUri("/oauth2/callback/*")
				.and()
				.userInfoEndpoint()
				.userService(customOAuth2UserService)
				.and()
				.successHandler(oAuth2AuthenticationSuccessHandler)
				.failureHandler(oAuth2AuthenticationFailureHandler);
		http.addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		return http.build();
	}
}