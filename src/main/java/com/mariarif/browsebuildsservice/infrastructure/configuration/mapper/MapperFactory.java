package com.mariarif.browsebuildsservice.infrastructure.configuration.mapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.DomainToResponseMapper;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.RequestToDomainMapper;

@Configuration
public class MapperFactory {

//	@Bean
//	public DomainToEntityMapper domainToEntityMapper() {
//		return new DomainToEntityMapper();
//	}

	@Bean
	public DomainToResponseMapper domainToResponseMapper() {
		return new DomainToResponseMapper();
	}

	@Bean
	public RequestToDomainMapper requestToDomainMapper() {
		return new RequestToDomainMapper();
	}

}
