package com.mariarif.browsebuildsservice.infrastructure.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import lombok.Data;

@Data
@EnableAsync
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {
	
	private List<String> allowedOrigins = new ArrayList<>();
	
	private List<String> authorizedRedirectUris = new ArrayList<>();
	
	private String tokenSecret;
	
	private long tokenExpirationMsec;
	
	private String invitationTokenSecret;
	
	private long invitationTokenExpirationMsec;
	
	private String baseUrl;
	
	private String clientUrl;
}
