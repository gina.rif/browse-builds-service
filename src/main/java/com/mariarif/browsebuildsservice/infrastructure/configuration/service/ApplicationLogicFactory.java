package com.mariarif.browsebuildsservice.infrastructure.configuration.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.azure.storage.blob.BlobServiceClient;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.CategoryServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.ClientServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.CompanyServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.FileServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.InvitationServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.MailServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.MessageServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.PictureServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.PostServiceImpl;
import com.mariarif.browsebuildsservice.applicationlogic.services.implementation.UserServiceImpl;
import com.mariarif.browsebuildsservice.applicationservices.services.CategoryService;
import com.mariarif.browsebuildsservice.applicationservices.services.ClientService;
import com.mariarif.browsebuildsservice.applicationservices.services.CompanyService;
import com.mariarif.browsebuildsservice.applicationservices.services.FileService;
import com.mariarif.browsebuildsservice.applicationservices.services.InvitationService;
import com.mariarif.browsebuildsservice.applicationservices.services.MailService;
import com.mariarif.browsebuildsservice.applicationservices.services.PictureService;
import com.mariarif.browsebuildsservice.applicationservices.services.PostService;
import com.mariarif.browsebuildsservice.applicationservices.services.UserService;
import com.mariarif.browsebuildsservice.infrastructure.configuration.AppConfig;
import com.mariarif.browsebuildsservice.infrastructure.configuration.MailConfig;
import com.mariarif.browsebuildsservice.infrastructure.configuration.property.FileStorageProperties;
import com.mariarif.browsebuildsservice.infrastructure.security.TokenProvider;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.CategoryRepository;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.ClientRepository;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.CompanyRepository;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.InvitationRepository;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.MessageRepository;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.PictureRepository;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.PostRepository;
import com.mariarif.browsebuildsservice.repositoryinterface.repository.UserRepository;

import lombok.NonNull;

@Configuration
public class ApplicationLogicFactory {

	@Bean
	public MailService mailService(AppConfig appConfig, MailConfig mailConfig) {
		return new MailServiceImpl(appConfig, mailConfig);
	}
	
	@Bean
	public FileService fileService(@NonNull FileStorageProperties fileStorageProperties, BlobServiceClient blobServiceClient) {
		return new FileServiceImpl(fileStorageProperties, blobServiceClient);
	}

	@Bean
	public CompanyService companyService(CompanyRepository companyRepository,UserService userService, MailService mailService,PictureService pictureService, CategoryService categoryService) {
		return new CompanyServiceImpl(companyRepository,userService, mailService, pictureService, categoryService);
	}

	@Bean
	public ClientService clientService(ClientRepository clientRepository,UserService userService, MailService mailService, PictureService pictureService) {
		return new ClientServiceImpl(clientRepository,userService, mailService, pictureService);
	}

	@Bean
	public InvitationService invitationService(InvitationRepository invitationRepository, TokenProvider tokenProvider) {
		return new InvitationServiceImpl(invitationRepository, tokenProvider);
	}
	
	@Bean
	public CategoryService categoryService(CategoryRepository categoryRepository) {
		return new CategoryServiceImpl(categoryRepository);
	}

	@Bean
	public UserService userService(UserRepository userRepository, TokenProvider tokenProvider) {
		return new UserServiceImpl(userRepository, tokenProvider);
	}

	@Bean
	public PostService postService(PostRepository postRepository, CategoryService categoryService,
	CompanyService companyService, ClientService clientService, PictureService pictureService) {
		return new PostServiceImpl(postRepository, categoryService, companyService, clientService, pictureService);
	}
	
	@Bean
	public PictureService pictureService(PictureRepository pictureRepository, FileService fileService) {
		return new PictureServiceImpl(pictureRepository, fileService);
	}
	
	@Bean
	public MessageServiceImpl messageService(MessageRepository messageRepository, CategoryService categoryService,
			UserService userService, MailService mailService) {
		return new MessageServiceImpl(messageRepository, categoryService, userService, mailService);
	}
}
