package com.mariarif.browsebuildsservice.infrastructure.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.beans.factory.annotation.Value;

import lombok.Data;

@Data
@Configuration
@EnableAsync
@ConfigurationProperties(prefix = "spring.mail")
public class MailConfig {
	private String host;

	private int port;

	private String username;

	private String password;

	@Value("${spring.mail.properties.mail.smtp.auth}")
	private String propertiesMailSmtpAuth;

	@Value("${spring.mail.properties.mail.smtp.starttls.enable}")
	private String propertiesMailSmtpStarttlsEnable;

}
