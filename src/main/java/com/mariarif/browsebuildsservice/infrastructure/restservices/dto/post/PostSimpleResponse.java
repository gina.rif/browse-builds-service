package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanySimpleResponse;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class PostSimpleResponse {
	@NotBlank
	private long id;

	@NotBlank
	private CompanySimpleResponse company;

	@NotBlank
	private String description;

	@NotBlank
	private Date datePosted;
}
