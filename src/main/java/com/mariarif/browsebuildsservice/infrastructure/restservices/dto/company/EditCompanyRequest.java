package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company;

import java.util.Arrays;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class EditCompanyRequest {
	@NotBlank
	private long id;
	@NotBlank
	private String phone;
	
	private MultipartFile profilePicture;
	@NotBlank
	private String companyName;
	@NotBlank
	private String description;
	@NotBlank
	private String taxId; // Legal Tax ID for the company
	@NotBlank
	private String registrationNumber; // Registration number for the company
	@NotBlank
	private String countryCode;
	@NotBlank
	private String county;
	@NotBlank
	private String city;
	@NotBlank
	private String street;
	@NotBlank
	private String number;
	private double latitude;
	private double longitude;
	private long[] categoryIds;
	
	public void setCategoryIds(long[] categoryIds) {
		if (categoryIds.length > 30) {
			throw new IllegalArgumentException("The length of the category ids array cannot exceed 30.");
		} else {
			this.categoryIds = Arrays.copyOf(categoryIds, categoryIds.length);
		}
	}
}
