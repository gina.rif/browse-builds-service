package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.picture.PictureResponse;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class PostResponse {
	@NotBlank
	private long id;

	@NotBlank
	private long companyId;

	@NotBlank
	private String description;

	private List<Long> categoryIds;

	private List<PictureResponse> picturesUrls;
}
