package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.picture;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class PictureResponse {
	private long id;
	private String url;
}
