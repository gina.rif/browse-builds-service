package com.mariarif.browsebuildsservice.infrastructure.restservices.dto;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategorySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.ClientSimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.PostSimpleResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class SearchResponse {

	List<CategorySimpleResponse> categories;
	List<ClientSimpleResponse> clients;
	List<CompanySimpleResponse> companies;
	List<PostSimpleResponse> posts;
}
