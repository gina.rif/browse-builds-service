package com.mariarif.browsebuildsservice.infrastructure.restservices.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.services.PostService;
import com.mariarif.browsebuildsservice.domain.model.AddressFilter;
import com.mariarif.browsebuildsservice.domain.model.Filter;
import com.mariarif.browsebuildsservice.domain.model.Post;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.NewPostRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.PostDetailedResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.PostResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.DomainToResponseMapper;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.RequestToDomainMapper;
import com.mariarif.browsebuildsservice.infrastructure.security.CurrentUser;
import com.mariarif.browsebuildsservice.infrastructure.security.UserPrincipal;

@RestController
@RequestMapping(path = "/posts")
//@CrossOrigin("http://localhost:5173")
public class PostController {

	@Autowired
	private final PostService postService;

	public PostController(PostService postService) {
		this.postService = postService;
	}

	@GetMapping("/{id}")
	public PostDetailedResponse getPostById(@PathVariable long id) throws BaseException {
		return DomainToResponseMapper.postToPostDetailedResponse(postService.getPostById(id));
	}

	@GetMapping
	public Page<PostDetailedResponse> getAllPostsFilteredAndSorted(@RequestParam(name = "page") int page,
			@RequestParam(name = "view") Short view,
			@RequestParam(name = "categoryIds", required = false) List<Long> categoryIds,
			@RequestParam(name = "companyIds", required = false) List<Long> companyIds,
			@RequestParam(name = "dateFrom", required = false) String dateFrom,
			@RequestParam(name = "dateTo", required = false) String dateTo,
			@RequestParam(name = "ratingAverages", required = false) List<Short> ratingAverages,
			@RequestParam(name = "addressFilters", required = false) JSONArray addressFilters,
			@RequestParam(name = "sort") String sort) throws BaseException, JSONException {

		List<AddressFilter> addressFiltersList = AddressFilter.parseAddressFilters(addressFilters);
		Filter filter = new Filter(dateFrom, dateTo, companyIds, categoryIds, addressFiltersList, ratingAverages);
		Page<Post> posts = postService.getAllPostsFilteredAndSorted(filter, sort, page, view);
		return posts.map(c -> DomainToResponseMapper.postToPostDetailedResponse(c));
	}

	@PreAuthorize("hasRole('COMPANY')")
	@PostMapping(consumes = "multipart/form-data")
	public PostResponse createNewPost(@CurrentUser UserPrincipal userPrincipal,
			@ModelAttribute NewPostRequest newPostRequest) {
		return DomainToResponseMapper.postToPostResponse(postService.createPost(
				RequestToDomainMapper.newPostRequestToPost(newPostRequest), userPrincipal.getUserId(),
				newPostRequest.getCategoryIds(), newPostRequest.getPictures()));
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('COMPANY')")
	public HttpStatusCode deletePostById(@CurrentUser UserPrincipal userPrincipal, @PathVariable long id)
			throws BaseException {
		try {
			postService.deletePostById(id);
			return HttpStatus.OK;
		} catch (Exception e) {
			return HttpStatus.NO_CONTENT;
		}
	}

	@GetMapping("/company/{companyId}")
	public ResponseEntity<Page<PostDetailedResponse>> getAllPostsByCompanyId(@RequestParam(name = "page") int page,
			@RequestParam(name = "view") Short view, @PathVariable long companyId) throws BaseException, JSONException {

		Page<Post> posts = postService.getAllPostsByCompanyId(companyId, page, view);
		if (posts.hasContent()) {
			return new ResponseEntity<>(posts.map(c -> DomainToResponseMapper.postToPostDetailedResponse(c)),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
}