package com.mariarif.browsebuildsservice.infrastructure.restservices.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.mariarif.browsebuildsservice.domain.model.Address;
import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.domain.model.Client;
import com.mariarif.browsebuildsservice.domain.model.Company;
import com.mariarif.browsebuildsservice.domain.model.Message;
import com.mariarif.browsebuildsservice.domain.model.Post;
import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.domain.model.UserType;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.SpecificUser;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.address.AddressRequestResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategoryResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategorySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.ClientResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.ClientSimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanyAsIdAndCompanyNameResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanyResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanyWithAddressAndCategoriesResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.message.MessageDetailedResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.picture.PictureResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.PostDetailedResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.PostResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.PostSimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.user.UserResponse;

public class DomainToResponseMapper {

	public static UserResponse userToUserResponse(User user) {
		UserType userType = UserType.valueOf(user.getUserType());
		if (userType.equals(UserType.COMPANY)) {
			return new UserResponse(user.getId(), "COMPANY", user.getProvider(), user.getEmail(),
					user.getEmailVerified(), user.getImageUrl(), companyToCompanyResponse(user.getCompany()));
		} else if (userType.equals(UserType.CLIENT)) {
			return new UserResponse(user.getId(), "CLIENT", user.getProvider(), user.getEmail(),
					user.getEmailVerified(), user.getImageUrl(), clientToClientResponse(user.getClient()));
		} else {
			return new UserResponse(user.getId(), "CLIENT", user.getProvider(), user.getEmail(),
					user.getEmailVerified(), user.getImageUrl(), null);
		}
	}

	public static CompanyResponse companyToCompanyResponse(Company company) {
		List<CategorySimpleResponse> categories =company.getCategories() != null ? company.getCategories().stream()
				.map(c -> categoryToCategorySimpleResponse(c)).toList() : null;
		return new CompanyResponse(company.getId(), company.getUser().getEmail(), company.getUser().getPhone(),
				company.getUser().getImageUrl(), company.getCompanyName(), company.getDescription(),
				addressToAddressResponse(company.getAddress()), company.getTaxId(), company.getRegistrationNumber(),
				categories);
	}

	public static CompanySimpleResponse companyToCompanySimpleResponse(Company company) {
		return new CompanySimpleResponse(company.getId(), company.getUser().getImageUrl(), company.getCompanyName());
	}

	public static CompanyAsIdAndCompanyNameResponse companyToCompanyAsIdAndCompanyNameResponse(
			Company company) {
		return new CompanyAsIdAndCompanyNameResponse(company.getId(), company.getCompanyName());
	}

	public static CompanyWithAddressAndCategoriesResponse companyToCompanyWithAddressAndCategoriesResponsee(Company company) {
		List<CategorySimpleResponse> categories = company.getCategories() != null ? company.getCategories().stream()
				.map(c -> categoryToCategorySimpleResponse(c)).toList() : null;
		return new CompanyWithAddressAndCategoriesResponse(company.getId(), company.getCompanyName(), addressToAddressResponse(company.getAddress()),categories);
	}
	
	public static AddressRequestResponse addressToAddressResponse(Address address) {
		return new AddressRequestResponse(address.getCounty(), address.getCity(),
				address.getStreet(), address.getNumber(), address.getLatitude(), address.getLongitude());
	}

	public static ClientResponse clientToClientResponse(Client client) {
		return new ClientResponse(client.getId(), client.getUser().getEmail(), client.getUser().getPhone(),
				client.getUser().getImageUrl(), client.getFirstName(), client.getLastName());
	}

	public static ClientSimpleResponse clientToClientSimpleResponse(Client client) {
		return new ClientSimpleResponse(client.getId(), client.getUser().getImageUrl(), client.getFirstName(),
				client.getLastName());
	}

	public static CategoryResponse categoryToCategoryResponse(Category category) {
		Long[] subcategoriesIds = category.getSubcategories().stream().map(s -> s.getId()).toArray(Long[]::new);
		long[] primitiveSubcategoriesIds = convertLongArraytoPrimitiveArray(subcategoriesIds);
		long parentCategoryId = category.getParentCategory() == null ? 0 : category.getParentCategory().getId();
		return new CategoryResponse(category.getId(), category.getName(), primitiveSubcategoriesIds, parentCategoryId);
	}

	public static CategorySimpleResponse categoryToCategorySimpleResponse(Category category) {
		long parentCategoryId = category.getParentCategory() == null ? 0 : category.getParentCategory().getId();
		return new CategorySimpleResponse(category.getId(), category.getName(), parentCategoryId);
	}

	public static PostResponse postToPostResponse(Post post) {
		List<Long> categoriesIds = post.getCategories() != null ? post.getCategories().stream().map(s -> s.getId()).toList() : null;
				post.getCategories().stream().map(s -> s.getId()).toList();

		List<PictureResponse> pictures = post.getPictures().stream()
				.map(p -> new PictureResponse(p.getId(), p.getPictureUrl())).collect(Collectors.toList());
		return new PostResponse(post.getId(), post.getCompany().getId(), post.getDescription(), categoriesIds, pictures);
	}

	public static PostSimpleResponse postToPostSimpleResponse(Post post) {
		CompanySimpleResponse company = companyToCompanySimpleResponse(post.getCompany());
		return new PostSimpleResponse(post.getId(), company, post.getDescription(), post.getDatePosted());
	}

	public static PostDetailedResponse postToPostDetailedResponse(Post post) {
		List<CategorySimpleResponse> categories = post.getCategories() != null ? post.getCategories().stream()
				.map(c -> categoryToCategorySimpleResponse(c)).toList() : null;
		CompanySimpleResponse company = companyToCompanySimpleResponse(post.getCompany());
//		ClientSimpleResponse taggedClient = post.getTaggedClient() == null ? null
//				: clientToClientSimpleResponse(post.getTaggedClient());
		List<PictureResponse> pictures = post.getPictures().stream()
				.map(p -> new PictureResponse(p.getId(), p.getPictureUrl())).collect(Collectors.toList());
		return new PostDetailedResponse(post.getId(), company, post.getDescription(), categories,
				pictures, post.getDatePosted());
	}

	public static MessageDetailedResponse messageToMessageDetailedResponse(Message message) {
		List<CategorySimpleResponse> categories = message.getCategories() != null ? message.getCategories().stream()
				.map(c -> categoryToCategorySimpleResponse(c)).toList() : null;
		SpecificUser fromUser = getSpecificUserFromUser(message.getFromUser());
		SpecificUser toUser = getSpecificUserFromUser(message.getToUser());

		return new MessageDetailedResponse(message.getId(), fromUser, toUser, message.getText(), message.getCounty(), message.getCity(), categories,
				message.getDateRequested(), message.getDate());
	}

	private static SpecificUser getSpecificUserFromUser(User user) {
		if (UserType.valueOf(user.getUserType()) == UserType.COMPANY) {
			return companyToCompanySimpleResponse(user.getCompany());
		} else if (UserType.valueOf(user.getUserType()) == UserType.CLIENT) {
			return clientToClientSimpleResponse(user.getClient());
		} else
			return null;
	}

	private static long[] convertLongArraytoPrimitiveArray(Long[] array) {
		long[] primitiveArray = new long[array.length];
		for (int i = 0; i < array.length; i++) {
			primitiveArray[i] = array[i];
		}
		return primitiveArray;
	}
}
