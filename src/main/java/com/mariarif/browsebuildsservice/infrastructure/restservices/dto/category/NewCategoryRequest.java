package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category;

import org.springframework.stereotype.Component;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class NewCategoryRequest {
	
	@NotBlank
	String name;
	
	long parentCategoryId;
}
