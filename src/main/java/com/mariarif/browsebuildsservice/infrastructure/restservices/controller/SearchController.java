package com.mariarif.browsebuildsservice.infrastructure.restservices.controller;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mariarif.browsebuildsservice.applicationservices.services.CategoryService;
import com.mariarif.browsebuildsservice.applicationservices.services.ClientService;
import com.mariarif.browsebuildsservice.applicationservices.services.CompanyService;
import com.mariarif.browsebuildsservice.applicationservices.services.PostService;
import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.domain.model.Client;
import com.mariarif.browsebuildsservice.domain.model.Company;
import com.mariarif.browsebuildsservice.domain.model.Post;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.SearchResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategorySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.ClientSimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.PostSimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.DomainToResponseMapper;

@RestController
@RequestMapping(path = "/search")
public class SearchController {
	@Autowired
	private final CategoryService categoryService;

	@Autowired
	private final CompanyService companyService;

	@Autowired
	private final ClientService clientService;

	@Autowired
	private final PostService postService;

	public SearchController(CategoryService categoryService, CompanyService companyService, ClientService clientService,
			PostService postService) {
		this.categoryService = categoryService;
		this.companyService = companyService;
		this.clientService = clientService;
		this.postService = postService;
	}

	@GetMapping
	public ResponseEntity<SearchResponse> searchAll(@RequestParam(name = "text") String text) {
		SearchResponse searchResponse = new SearchResponse();
		CompletableFuture<List<Category>> categoriesFuture = CompletableFuture
				.supplyAsync(() -> categoryService.getCategoriesLike(text));

		CompletableFuture<List<Client>> clientsFuture = CompletableFuture
				.supplyAsync(() -> clientService.getClientsLike(text));

		CompletableFuture<List<Company>> companiesFuture = CompletableFuture
				.supplyAsync(() -> companyService.getCompaniesLike(text));

		CompletableFuture<List<Post>> postsFuture = CompletableFuture.supplyAsync(() -> postService.getPostsLike(text));

		try {
			searchResponse = CompletableFuture.allOf(categoriesFuture, clientsFuture, companiesFuture, postsFuture)
					.thenApplyAsync((Void) -> {

						List<CategorySimpleResponse> categories;
						try {
							categories = categoriesFuture.get().stream()
									.map(c -> DomainToResponseMapper.categoryToCategorySimpleResponse(c)).toList();
						} catch (InterruptedException | ExecutionException ex) {
							return null;
						}
						List<ClientSimpleResponse> clients;
						try {
							clients = clientsFuture.get().stream()
									.map(c -> DomainToResponseMapper.clientToClientSimpleResponse(c)).toList();
						} catch (InterruptedException | ExecutionException ex) {
							return null;
						}
						List<CompanySimpleResponse> companies;
						try {
							companies = companiesFuture.get().stream()
									.map(c -> DomainToResponseMapper.companyToCompanySimpleResponse(c)).toList();
						} catch (InterruptedException | ExecutionException ex) {
							return null;
						}
						List<PostSimpleResponse> posts;
						try {
							posts = postsFuture.get().stream()
									.map(c -> DomainToResponseMapper.postToPostSimpleResponse(c)).toList();
						} catch (InterruptedException | ExecutionException ex) {
							return null;
						}

						SearchResponse sr = new SearchResponse(categories, clients, companies, posts);
						return sr;
					}).get();
		} catch (InterruptedException | ExecutionException ex) {
			new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<>(searchResponse, HttpStatus.OK);

	}

}
