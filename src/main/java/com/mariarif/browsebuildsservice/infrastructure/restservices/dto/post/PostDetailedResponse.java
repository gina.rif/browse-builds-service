package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategorySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.ClientSimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.picture.PictureResponse;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class PostDetailedResponse {
	@NotBlank
	private long id;

	@NotBlank
	private CompanySimpleResponse company;

	@NotBlank
	private String description;

	private List<CategorySimpleResponse> categories;

	private List<PictureResponse> pictures;
	
	private Date datePosted;
}
