package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company;

import java.util.Arrays;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.address.AddressRequestResponse;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
@Component
@AllArgsConstructor
public class NewCompanyRequest {
	
	private String email;
	private String provider;
	private String phone;
//	private String imageUrl;
	private String companyName;
	private String description;
	private AddressRequestResponse address;
	private String taxId; // Legal Tax ID for the company
	private String registrationNumber; // Registration number for the company
	
	@Size(max = 30)
	private long[] categoryIds;
	
	public NewCompanyRequest() {
		categoryIds = new long[1];
	}
	
	public void setCategoryIds(long[] categoryIds) {
		if (categoryIds.length > 30) {
			throw new IllegalArgumentException("The length of the category ids array cannot exceed 30.");
		} else {
			this.categoryIds = Arrays.copyOf(categoryIds, categoryIds.length);
		}
	}
}
