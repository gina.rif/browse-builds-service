package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class NewClientRequest {

	private String email;
	private String provider;
	private String phone;
//	private String imageUrl;
	private String firstName;
	private String lastName;
}
