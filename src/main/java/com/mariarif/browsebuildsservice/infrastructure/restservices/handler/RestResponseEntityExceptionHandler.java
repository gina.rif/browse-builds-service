package com.mariarif.browsebuildsservice.infrastructure.restservices.handler;

import java.io.InvalidObjectException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.CategoryExistsException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.CategoryNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.ClientExistsException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.ClientNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.CompanyExistsException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.CompanyNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.InvitationNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.InvitationUsedException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.PostExistsException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.PostNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserExistsException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserVerifiedException;

import lombok.extern.log4j.Log4j2;

@Log4j2
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { CategoryNotFoundException.class, ClientNotFoundException.class,
			CompanyNotFoundException.class, InvitationNotFoundException.class, PostNotFoundException.class,
			UserNotFoundException.class })
	protected ResponseEntity<Object> handleNotFoundExceptions(RuntimeException ex, WebRequest request) {
		if (log.isWarnEnabled()) {
			log.warn(ex.getMessage(), ex);
		}
		return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = { CategoryExistsException.class, ClientExistsException.class,
			CompanyExistsException.class, PostExistsException.class, UserExistsException.class })
	protected ResponseEntity<Object> handleExistsExceptions(RuntimeException ex, WebRequest request) {
		if (log.isWarnEnabled()) {
			log.warn(ex.getMessage(), ex);
		}
		return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(value = { UserVerifiedException.class })
	protected ResponseEntity<Object> handleUserVerifiedExceptions(RuntimeException ex, WebRequest request) {
		if (log.isWarnEnabled()) {
			log.warn(ex.getMessage(), ex);
		}
		return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(value = { InvitationUsedException.class })
	protected ResponseEntity<Object> handleInvitationUsedExceptions(RuntimeException ex, WebRequest request) {
		if (log.isWarnEnabled()) {
			log.warn(ex.getMessage(), ex);
		}
		return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(InvalidObjectException.class)
	  public ResponseEntity<String> handleInvalidObjectException(InvalidObjectException ex) {
		if (log.isWarnEnabled()) {
			log.warn(ex.getMessage(), ex);
		}
	    return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(ex.getMessage());
	  }

	@ExceptionHandler(MaxUploadSizeExceededException.class)
	  public ResponseEntity<String> handleMaxSizeException(MaxUploadSizeExceededException ex) {
		if (log.isWarnEnabled()) {
			log.warn(ex.getMessage(), ex);
		}
	    return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("File too large!");
	  }
	
	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		if (log.isWarnEnabled()) {
			log.warn(ex.getMessage(), ex);
		}
		return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT, request);
	}
	
	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<Object> handleOtherExceptions(Exception ex, WebRequest request) {
		if (log.isWarnEnabled()) {
			log.warn(ex.getMessage(), ex);
		}
		return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
