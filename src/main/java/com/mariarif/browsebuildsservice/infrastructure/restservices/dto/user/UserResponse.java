package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.user;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.SpecificUser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
	private long id;

	private String userType;
	
	private String provider;

	private String email;

	private Boolean emailVerified;
	
	private String imageUrl;

	private SpecificUser user;
}
