package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.message;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.SpecificUser;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategorySimpleResponse;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class MessageDetailedResponse {
	@NotBlank
	private long id;

	@NotBlank
	private SpecificUser fromUser;

	@NotBlank
	private SpecificUser toUser;
	
	@NotBlank
	private String text;
	
	@NotBlank
	private String county;
	
	@NotBlank
	private String city;

	private List<CategorySimpleResponse> categories;

	@NotBlank
	private Date dateRequested;
	
	@NotBlank
	private Date date;
}
