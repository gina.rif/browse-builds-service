package com.mariarif.browsebuildsservice.infrastructure.restservices.mapper;

import java.util.Date;

import com.mariarif.browsebuildsservice.domain.model.Address;
import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.domain.model.Client;
import com.mariarif.browsebuildsservice.domain.model.Company;
import com.mariarif.browsebuildsservice.domain.model.Message;
import com.mariarif.browsebuildsservice.domain.model.Post;
import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.domain.model.UserType;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.address.AddressRequestResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.NewCategoryRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.EditClientRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.NewClientRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.EditCompanyRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.NewCompanyRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.message.NewMessageRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post.NewPostRequest;

public class RequestToDomainMapper {

	public static Company newCompanyRequestToCompany(NewCompanyRequest newCompanyRequest) {
		User user = new User();
		user.setEmail(newCompanyRequest.getEmail());
		user.setPhone(newCompanyRequest.getPhone());
		user.setEmailVerified(false);
		user.setUserType(UserType.COMPANY.toString());
		user.setProvider(newCompanyRequest.getProvider());
		
		Address address = newAddressRequestToAddress(newCompanyRequest.getAddress());

		Company company = new Company();
		company.setUser(user);
		company.setCompanyName(newCompanyRequest.getCompanyName());
		company.setDescription(newCompanyRequest.getDescription());
		company.setAddress(address);
		company.setTaxId(newCompanyRequest.getTaxId());
		company.setRegistrationNumber(newCompanyRequest.getRegistrationNumber());
		return company;
	}

	public static Company editCompanyRequestToCompany(EditCompanyRequest editCompanyRequest) {
		Address address = new Address(editCompanyRequest.getId(),
				null,
				editCompanyRequest.getCounty(),
				editCompanyRequest.getCity(),
				editCompanyRequest.getStreet(),
				editCompanyRequest.getNumber(),
				editCompanyRequest.getLatitude(),
				editCompanyRequest.getLongitude()); 
		Company company = new Company();
		company.setId(editCompanyRequest.getId());
		company.setCompanyName(editCompanyRequest.getCompanyName());
		company.setDescription(editCompanyRequest.getDescription());
		company.setAddress(address);
		company.setTaxId(editCompanyRequest.getTaxId());
		company.setRegistrationNumber(editCompanyRequest.getRegistrationNumber());
		return company;
	}

	public static Address newAddressRequestToAddress(AddressRequestResponse addressRequestResponse) {
		return new Address(addressRequestResponse.getCounty(),
				addressRequestResponse.getCity(), addressRequestResponse.getStreet(),
				addressRequestResponse.getNumber(),
				addressRequestResponse.getLatitude(),
				addressRequestResponse.getLongitude()); 
	}

	public static Client newClientRequestToClient(NewClientRequest newClientRequest) {
		User user = new User();
		user.setEmail(newClientRequest.getEmail());
		user.setPhone(newClientRequest.getPhone());
		user.setEmailVerified(false);
		user.setUserType(UserType.CLIENT.toString());
		user.setProvider(newClientRequest.getProvider());
		
		Client client = new Client();
		client.setUser(user);
		client.setFirstName(newClientRequest.getFirstName());
		client.setLastName(newClientRequest.getLastName());
		return client;
	}
	
	public static Client editClientRequestToClient(EditClientRequest editClientRequest) {
		Client client = new Client();
		client.setId(editClientRequest.getId());
		client.setFirstName(editClientRequest.getFirstName());
		client.setLastName(editClientRequest.getLastName());
		return client;
	}
	public static Category newCategoryRequestToCategory(NewCategoryRequest newCategoryRequest) {
		Category parentCategory = newCategoryRequest.getParentCategoryId() == 0 ? null
				: new Category(newCategoryRequest.getParentCategoryId());
		Category category = new Category();
		category.setName(newCategoryRequest.getName());
		parentCategory.addSubcategory(category);
		return category;
	}

	public static Post newPostRequestToPost(NewPostRequest newPostRequest) {

		return new Post(newPostRequest.getDescription(), new Date());
	}
	
	public static Message newMessageRequestToMessage(NewMessageRequest newMessageRequest) {
		Message message = new Message();
		message.setText(newMessageRequest.getText());
		message.setCounty(newMessageRequest.getCounty());
		message.setCity(newMessageRequest.getCity());
		message.setDateRequested(newMessageRequest.getDateRequested());
		message.setDate(new Date());
		return message;
	}
}
