package com.mariarif.browsebuildsservice.infrastructure.restservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.services.CategoryService;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategoryResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategorySimpleResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.NewCategoryRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.DomainToResponseMapper;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.RequestToDomainMapper;
@RestController
@RequestMapping(path = "/categories")
//@CrossOrigin("http://localhost:5173")
public class CategoryController {

	@Autowired
	private final CategoryService categoryService;

	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@GetMapping("/{id}")
	public CategoryResponse getCategoryById(@PathVariable long id) throws BaseException {
		return DomainToResponseMapper.categoryToCategoryResponse(categoryService.getCategoryById(id));
	}

	@GetMapping
	public List<CategorySimpleResponse> getAllCategories() throws BaseException {
		return categoryService.getAllCategories().stream().map(c -> DomainToResponseMapper.categoryToCategorySimpleResponse(c)).toList();
	}

	@PutMapping
	public CategoryResponse createNewCategory(@RequestBody NewCategoryRequest request) throws BaseException {
		return DomainToResponseMapper.categoryToCategoryResponse(
				categoryService.createCategory(RequestToDomainMapper.newCategoryRequestToCategory(request)));
	}
}