package com.mariarif.browsebuildsservice.infrastructure.restservices.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.services.ClientService;
import com.mariarif.browsebuildsservice.domain.model.Client;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.ClientResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.EditClientRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client.NewClientRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.DomainToResponseMapper;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.RequestToDomainMapper;
import com.mariarif.browsebuildsservice.infrastructure.security.CurrentUser;
import com.mariarif.browsebuildsservice.infrastructure.security.UserPrincipal;

@RestController
@RequestMapping(path = "/clients")
//@CrossOrigin("http://localhost:5173")
public class ClientController {

	@Autowired
	private final ClientService clientService;

	public ClientController(ClientService clientService) {
		this.clientService = clientService;
	}

	@PostMapping
	public ResponseEntity<Object> createNewClient(@RequestBody NewClientRequest request) throws BaseException {
		Client client =clientService.createClient(RequestToDomainMapper.newClientRequestToClient(request));
		return new ResponseEntity<>(HttpStatus.CREATED);
		//		return clientService.createClient(RequestToDomainMapper.newClientRequestToClient(request)).getUser().getEmail();
	}
	
	@PreAuthorize("hasRole('CLIENT')")
    @PutMapping(consumes = "multipart/form-data")
    public ClientResponse editClient(@CurrentUser UserPrincipal userPrincipal, @ModelAttribute EditClientRequest request) throws BaseException, AccessDeniedException, IOException {
        if(userPrincipal.getUserId() == request.getId()) {
			 return DomainToResponseMapper.clientToClientResponse(clientService.editClient(RequestToDomainMapper.editClientRequestToClient(request), request.getPhone(),request.getProfilePicture()));
        } else {
        	throw new AccessDeniedException("User is not allowed to edit another account!");
        }
    }
}