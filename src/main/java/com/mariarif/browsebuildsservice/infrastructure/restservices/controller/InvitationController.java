package com.mariarif.browsebuildsservice.infrastructure.restservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.services.InvitationService;

@RestController
@RequestMapping(path = "/invitations")
public class InvitationController {
	@Autowired
	private final InvitationService invitationService;

	public InvitationController(InvitationService invitationService) {
		this.invitationService = invitationService;
	}

	@GetMapping("/activate")
	public ResponseEntity<Object> useInvitation(@RequestParam(name = "invitationToken") String invitationToken) {
		try {
			invitationService.useInvitation(invitationToken);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		} catch (BaseException e) {
			//TODO: different response based on error
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
