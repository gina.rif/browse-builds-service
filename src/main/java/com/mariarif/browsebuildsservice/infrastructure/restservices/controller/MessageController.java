package com.mariarif.browsebuildsservice.infrastructure.restservices.controller;

import java.io.InvalidObjectException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.services.MessageService;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.message.MessageDetailedResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.message.NewMessageRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.DomainToResponseMapper;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.RequestToDomainMapper;
import com.mariarif.browsebuildsservice.infrastructure.security.CurrentUser;
import com.mariarif.browsebuildsservice.infrastructure.security.UserPrincipal;

@RestController
@RequestMapping(path = "/messages")
//@CrossOrigin("http://localhost:5173")
public class MessageController {

	@Autowired
	private final MessageService messageService;

	public MessageController(MessageService messageService) {
		this.messageService = messageService;
	}

	@GetMapping("/{id}")
	public MessageDetailedResponse getMessageById(@CurrentUser UserPrincipal userPrincipal, @PathVariable long id) throws BaseException {
		return DomainToResponseMapper.messageToMessageDetailedResponse(messageService.getMessageById(id, userPrincipal.getUserId()));
	}

	@PreAuthorize("hasRole('USER')")
	@PostMapping
	public MessageDetailedResponse createNewMessage(@CurrentUser UserPrincipal userPrincipal,
			@RequestBody NewMessageRequest newMessageRequest) throws InvalidObjectException, BaseException {
		return DomainToResponseMapper.messageToMessageDetailedResponse(messageService.createMessage(
				RequestToDomainMapper.newMessageRequestToMessage(newMessageRequest), userPrincipal.getUserId(),
				newMessageRequest.getToUserId(), newMessageRequest.getCategoryIds()));
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public HttpStatusCode deleteMessageById(@CurrentUser UserPrincipal userPrincipal, @PathVariable long id)
			throws BaseException {
		try {
			messageService.deleteMessageById(id, userPrincipal.getUserId());
			return HttpStatus.OK;
		} catch (Exception e) {
			return HttpStatus.NO_CONTENT;
		}
	}
}