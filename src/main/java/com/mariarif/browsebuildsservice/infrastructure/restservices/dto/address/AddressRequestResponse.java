package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.address;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class AddressRequestResponse {
	private String county;
	
	private String city;
	
	private String street;

	private String number;
	
	private double latitude;
	
	private double longitude;
}
