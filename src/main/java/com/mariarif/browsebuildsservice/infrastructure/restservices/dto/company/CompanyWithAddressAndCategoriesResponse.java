package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.SpecificUser;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.address.AddressRequestResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategorySimpleResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class CompanyWithAddressAndCategoriesResponse implements SpecificUser{
	
	private long id;
	private String companyName;
	private AddressRequestResponse address;
	private List<CategorySimpleResponse> categories;

}
