package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.SpecificUser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class CompanyAsIdAndCompanyNameResponse implements SpecificUser{
	
	private long id;
	private String companyName;

}
