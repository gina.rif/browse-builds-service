package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.message;

import java.sql.Date;
import java.util.Arrays;

import org.springframework.stereotype.Component;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Component
@AllArgsConstructor
public class NewMessageRequest {
	@NotBlank
	private long toUserId;
	
	@NotBlank
	private String text;
	
	@NotBlank
	private String county;
	
	@NotBlank
	private String city;
	
	@Size(max = 30)
	private long[] categoryIds;

	private Date dateRequested;

	public NewMessageRequest() {
		this.categoryIds = new long[1];
	}

	public void setCategoryIds(long[] categoryIds) {
		if (categoryIds.length > 30) {
			throw new IllegalArgumentException("The length of the category ids array cannot exceed 30.");
		} else {
			this.categoryIds = Arrays.copyOf(categoryIds, categoryIds.length);
		}
	}
}
