package com.mariarif.browsebuildsservice.infrastructure.restservices.controller;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.services.CompanyService;
import com.mariarif.browsebuildsservice.domain.model.AddressFilter;
import com.mariarif.browsebuildsservice.domain.model.Company;
import com.mariarif.browsebuildsservice.domain.model.Filter;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanyAsIdAndCompanyNameResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanyResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.CompanyWithAddressAndCategoriesResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.EditCompanyRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company.NewCompanyRequest;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.DomainToResponseMapper;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.RequestToDomainMapper;
import com.mariarif.browsebuildsservice.infrastructure.security.CurrentUser;
import com.mariarif.browsebuildsservice.infrastructure.security.UserPrincipal;

@RestController
@RequestMapping(path = "/companies")
public class CompanyController {
	@Autowired
	private final CompanyService companyService;

	public CompanyController(CompanyService companyService) {
		this.companyService = companyService;
	}

	@PostMapping
	public ResponseEntity<Object> createNewCompany(@RequestBody NewCompanyRequest request) throws BaseException {
			Company company =companyService.createCompany(RequestToDomainMapper.newCompanyRequestToCompany(request), request.getCategoryIds());
			return new ResponseEntity<>(HttpStatus.CREATED);
//		return  companyService.createCompany(RequestToDomainMapper.newCompanyRequestToCompany(request), request.getCategoryIds());
	}

	@PreAuthorize("hasRole('COMPANY')")
    @PutMapping(consumes = "multipart/form-data")
    public CompanyResponse editCompany(@CurrentUser UserPrincipal userPrincipal, @ModelAttribute EditCompanyRequest request) throws BaseException, AccessDeniedException, IOException {
        if(userPrincipal.getUserId() == request.getId()) {
			 return DomainToResponseMapper.companyToCompanyResponse(companyService.editCompany(RequestToDomainMapper.editCompanyRequestToCompany(request), request.getPhone(),request.getProfilePicture(), request.getCategoryIds()));
        } else {
        	throw new AccessDeniedException("User is not allowed to edit another account!");
        }
    }

	@GetMapping("/v1")
	public List<CompanyAsIdAndCompanyNameResponse> getAllCompaniesAsIdAndCompanyNameResponse() throws BaseException {
		return companyService.getAllCompanies().stream().map(c -> DomainToResponseMapper.companyToCompanyAsIdAndCompanyNameResponse(c)).toList();
	}
	
	@GetMapping("/v2")
	public List<CompanyWithAddressAndCategoriesResponse> getAllCompaniesWithAddressAndCategoriesResponse() throws BaseException {
		return companyService.getAllCompanies().stream().map(c -> DomainToResponseMapper.companyToCompanyWithAddressAndCategoriesResponsee(c)).toList();
	}
	
	@GetMapping
	public Page<CompanyResponse> getAllCompaniesFilteredAndSorted(@RequestParam(name = "page") int page,
			@RequestParam(name = "view") Short view,
			@RequestParam(name = "categoryIds", required = false) List<Long> categoryIds,
		@RequestParam(name = "ratingAverages", required = false) List<Short> ratingAverages,
		@RequestParam(name = "addressFilters", required = false) JSONArray addressFilters,
		@RequestParam(name = "sort") String sort) throws BaseException, JSONException {

	List<AddressFilter> addressFiltersList = AddressFilter.parseAddressFilters(addressFilters);
	
	Filter filter = new Filter(null, null, null, categoryIds,addressFiltersList, ratingAverages);
	Page<Company> companies=companyService.getAllCompaniesFilteredAndSorted(filter, sort, page, view);
	return companies.map(c -> DomainToResponseMapper.companyToCompanyResponse(c));

	}
        //    @DeleteMapping(path = "/{id}")
//    public Company delete(@PathVariable(name = "id") String companyId) throws BaseException {
//        return companyService.deleteCompany(companyId);
//    }
//
//    @GetMapping(path = "/{id}")
//    public Company findById(@PathVariable(name = "id") String companyId) throws BaseException {
//        return companyService.findCompanyById(companyId);
//    }
}