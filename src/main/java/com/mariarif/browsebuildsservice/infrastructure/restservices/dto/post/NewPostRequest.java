package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.post;

import java.util.Arrays;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Component
@AllArgsConstructor
public class NewPostRequest {
	@NotBlank
	private long companyId;

	@NotBlank
	private String description;

	@Size(max = 30)
	private long[] categoryIds;

	@Size(max = 10)
	private MultipartFile[] pictures;

	public NewPostRequest() {
		this.categoryIds = new long[1];
		this.pictures = new MultipartFile[1];
	}

	public void setPictures(MultipartFile[] pictures) {
		if (pictures.length > 10) {
			throw new IllegalArgumentException("The length of the pictures array cannot exceed 10.");
		} else {
			this.pictures = Arrays.copyOf(pictures, pictures.length);
		}
	}

	public void setCategoryIds(long[] categoryIds) {
		if (categoryIds.length > 30) {
			throw new IllegalArgumentException("The length of the category ids array cannot exceed 30.");
		} else {
			this.categoryIds = Arrays.copyOf(categoryIds, categoryIds.length);
		}
	}
}
