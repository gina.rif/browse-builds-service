package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class EditClientRequest {
	@NotBlank
	private long id;
	@NotBlank
	private String phone;
	
	private MultipartFile profilePicture;
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;

}
