package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.client;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.SpecificUser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class ClientSimpleResponse implements SpecificUser{

	private long id;
	private String imageUrl;
	private String firstName;
	private String lastName;
}
