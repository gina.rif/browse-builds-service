package com.mariarif.browsebuildsservice.infrastructure.restservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mariarif.browsebuildsservice.applicationservices.services.UserService;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.user.UserResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.mapper.DomainToResponseMapper;
import com.mariarif.browsebuildsservice.infrastructure.security.CurrentUser;
import com.mariarif.browsebuildsservice.infrastructure.security.UserPrincipal;

@RestController
@RequestMapping(path = "/users")
public class UserController {
	@Autowired
	private final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

//	@GetMapping("/invitation")
////	@PreAuthorize("hasRole('USER')")
//	public HttpStatusCode activateAccount(@CurrentUser UserPrincipal userPrincipal) {
//		try {
//			userService.activateAccount(userPrincipal.getProviderId());
//			return HttpStatus.ACCEPTED;
//		} catch (Exception e) {
//			return HttpStatus.BAD_REQUEST;
//		}
//
//	}

	@GetMapping("/profile")
	@PreAuthorize("hasRole('USER')")
	public UserResponse getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
		return DomainToResponseMapper.userToUserResponse(userService.getUserByProviderId(userPrincipal.getProviderId()));
	}
	
	@GetMapping("/{id}")
//	@PreAuthorize("hasRole('USER')")
	public UserResponse getUserById(@PathVariable(name="id") long id) {
		return DomainToResponseMapper.userToUserResponse(userService.getUserById(id));
	}

	@DeleteMapping()
	@PreAuthorize("hasRole('USER')")
	public HttpStatusCode deleteAccount(@CurrentUser UserPrincipal userPrincipal) {
		try {
			userService.deleteAccount(userPrincipal.getProviderId());
			return HttpStatus.OK;
		} catch (Exception e) {
			return HttpStatus.NO_CONTENT;
		}

	}
}
