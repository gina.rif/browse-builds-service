package com.mariarif.browsebuildsservice.infrastructure.restservices.dto.company;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.SpecificUser;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.address.AddressRequestResponse;
import com.mariarif.browsebuildsservice.infrastructure.restservices.dto.category.CategorySimpleResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class CompanyResponse implements SpecificUser{
	
	private long id;
	private String email;
	private String phone;
	private String imageUrl;
	private String companyName;
	private String description;
	private AddressRequestResponse address;
	private String taxId; // Legal Tax ID for the company
	private String registrationNumber; // Registration number for the company
	private List<CategorySimpleResponse> categories;
}
