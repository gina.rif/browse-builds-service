package com.mariarif.browsebuildsservice.infrastructure.security.oauth2.user;

import java.util.Map;

public abstract class OAuth2UserInfo {
	protected Map<String, Object> attributes;

    public OAuth2UserInfo(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public abstract String getOAuth2Id();

//    public abstract String getName();

    public abstract String getEmail();

    public abstract String getImageUrl();
}
