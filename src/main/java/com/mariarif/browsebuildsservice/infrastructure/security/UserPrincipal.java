package com.mariarif.browsebuildsservice.infrastructure.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.domain.model.UserType;

public class UserPrincipal implements OAuth2User, UserDetails {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8171675735673620085L;
	
	private long userId;
	private String providerId;
    private String email;
    private String password;
    private Collection<? extends GrantedAuthority> authorities;
    private Map<String, Object> attributes;

    public UserPrincipal(long userId, String providerId, String email, String password, Collection<? extends GrantedAuthority> authorities) {
        this.userId = userId;
        this.providerId = providerId;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }

    public UserPrincipal(String providerId, String email, String password, Collection<? extends GrantedAuthority> authorities) {
        this.providerId = providerId;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }
    
    public static UserPrincipal create(User user) {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        UserType userType = UserType.valueOf(user.getUserType());
        switch(userType) {
        case CLIENT: 
        	authorities.add(new SimpleGrantedAuthority("ROLE_CLIENT"));
        	break;
        case COMPANY:
        	authorities.add(new SimpleGrantedAuthority("ROLE_COMPANY"));
        	break;
        default:
        	break;
        }

        return new UserPrincipal(
        		user.getId(),
                user.getProviderId(),
                user.getEmail(),
                null,
                authorities
        );
    }

    public static UserPrincipal create(User user, Map<String, Object> attributes) {
        UserPrincipal userPrincipal = UserPrincipal.create(user);
        userPrincipal.setAttributes(attributes);
        return userPrincipal;
    }

    public long getUserId() {
        return userId;
    }
    
    public String getProviderId() {
        return providerId;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String getName() {
        return String.valueOf(providerId);
    }
}
