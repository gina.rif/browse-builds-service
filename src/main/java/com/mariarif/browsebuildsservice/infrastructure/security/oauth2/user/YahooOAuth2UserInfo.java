package com.mariarif.browsebuildsservice.infrastructure.security.oauth2.user;

import java.util.Map;

public class YahooOAuth2UserInfo extends OAuth2UserInfo {
	
	public YahooOAuth2UserInfo(Map<String, Object> attributes) {
		super(attributes);
    }
	public Map<String, Object> getAttributes() {
        return attributes;
    }

    public String getOAuth2Id() {
        return (String) attributes.get("sub");
    }

//    public String getName() {
//        return (String) attributes.get("name");
//    }

    public String getEmail() {
        return (String) attributes.get("email");
    }

    public String getImageUrl() {
        return (String) attributes.get("picture");
    }
}
