package com.mariarif.browsebuildsservice.infrastructure.security.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserNotFoundException;
import com.mariarif.browsebuildsservice.applicationservices.services.UserService;
import com.mariarif.browsebuildsservice.domain.model.AuthProvider;
import com.mariarif.browsebuildsservice.domain.model.User;
import com.mariarif.browsebuildsservice.infrastructure.security.UserPrincipal;
import com.mariarif.browsebuildsservice.infrastructure.security.oauth2.exceptions.OAuth2AuthenticationProcessingException;
import com.mariarif.browsebuildsservice.infrastructure.security.oauth2.user.OAuth2UserInfo;
import com.mariarif.browsebuildsservice.infrastructure.security.oauth2.user.OAuth2UserInfoFactory;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@RequiredArgsConstructor
public class CustomOAuth2UserService extends DefaultOAuth2UserService {
	@Autowired
	private UserService userService;

	@Override
	public OAuth2User loadUser(OAuth2UserRequest oauth2UserRequest) throws OAuth2AuthenticationException {
		OAuth2User oauth2User = super.loadUser(oauth2UserRequest);

		try {
			return processOAuth2User(oauth2UserRequest, oauth2User);
		} catch (AuthenticationException ex) {
			log.error("An AuthenticationException occured when processing the OAuth2User! " + ex.getMessage(), ex,
					oauth2UserRequest);
			throw ex;
		} catch (Exception ex) {
			log.error("An exception occured when processing the OAuth2User! " + ex.getMessage(), ex, oauth2UserRequest);
			throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
		}
	}

	private OAuth2User processOAuth2User(OAuth2UserRequest oauth2UserRequest, OAuth2User oauth2User) {
		OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(
				oauth2UserRequest.getClientRegistration().getRegistrationId(), oauth2User.getAttributes());

		if (oAuth2UserInfo.getEmail().isEmpty()) {
			throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
		}

		AuthProvider requestProvider = AuthProvider
				.valueOf(oauth2UserRequest.getClientRegistration().getRegistrationId());

		try {
			User user = userService.getUserByEmail(oAuth2UserInfo.getEmail());
			if (requestProvider.equals(AuthProvider.facebook)
					|| user.getProvider().equalsIgnoreCase(requestProvider.toString())) {
				user = updateExistingUser(user, oAuth2UserInfo);
				return UserPrincipal.create(user);
			} else {
				throw new OAuth2AuthenticationProcessingException(
						"Looks like you're signed up with " + user.getProvider() + " account. Please use your "
								+ user.getProvider() + " account to login.");
			}
		} catch (UserNotFoundException ex) {
			throw new UserNotFoundException("User with email " + oAuth2UserInfo.getEmail() + " does not exist!");
		}
	}

	private User updateExistingUser(User existingUser, OAuth2UserInfo oAuth2UserInfo) {
//		existingUser.setName(oAuth2UserInfo.getName());
		existingUser.setProviderId(oAuth2UserInfo.getOAuth2Id());
		log.debug("updating existent user: " + oAuth2UserInfo.getOAuth2Id());
		return userService.editUser(existingUser);
	}
}
