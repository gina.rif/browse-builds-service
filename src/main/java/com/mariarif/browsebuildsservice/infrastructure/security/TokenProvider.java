package com.mariarif.browsebuildsservice.infrastructure.security;

import java.util.Date;
import java.util.UUID;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.mariarif.browsebuildsservice.infrastructure.configuration.AppConfig;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class TokenProvider {
	
	private final AppConfig appConfig;

	public TokenProvider(AppConfig appConfig) {
		this.appConfig = appConfig;
	}

	public String createToken(Authentication authentication) {
		UserPrincipal userPrincipal = (UserPrincipal) (authentication.getPrincipal());
		Date now = new Date();
		Date expiryDate = new Date(now.getTime() + appConfig.getTokenExpirationMsec());

		return Jwts.builder()
				.setSubject((userPrincipal.getProviderId()))
				.setIssuedAt(new Date())
				.setExpiration(expiryDate)
				.signWith(SignatureAlgorithm.HS512, appConfig.getTokenSecret())
				.compact();
	}

	public String createInvitationToken(String userEmail) {
		Date now = new Date();
		Date expiryDate = new Date(now.getTime() + appConfig.getInvitationTokenExpirationMsec());

		return Jwts.builder()
				.setId(UUID.randomUUID().toString())
				.setSubject(userEmail)
				.setIssuedAt(new Date())
				.setExpiration(expiryDate)
				.signWith(SignatureAlgorithm.HS512, appConfig.getInvitationTokenSecret())
				.compact();
	}
	
	public String getUserIdFromToken(String token) {
		Claims claims = Jwts.parser().setSigningKey(appConfig.getTokenSecret()).parseClaimsJws(token).getBody();

		return claims.getSubject();
	}
	
	public String getUserEmailFromInvitationToken(String invitationToken) {
		Claims claims = Jwts.parser().setSigningKey(appConfig.getInvitationTokenSecret()).parseClaimsJws(invitationToken).getBody();

		return claims.getSubject();
	}

	public boolean validateToken(String authToken) {
		try {
			log.debug("validating token...");
			Jwts.parser().setSigningKey(appConfig.getTokenSecret()).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException ex) {
			log.error("Invalid JWT signature");
		} catch (MalformedJwtException ex) {
			log.error("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			log.error("Expired JWT token");
		} catch (UnsupportedJwtException ex) {
			log.error("Unsupported JWT token");
		} catch (IllegalArgumentException ex) {
			log.error("JWT claims string is empty.");
		}
		return false;
	}
	
	public boolean validateInvitationToken(String invitationToken) {
		try {
			Jwts.parser().setSigningKey(appConfig.getInvitationTokenSecret()).parseClaimsJws(invitationToken);
			return true;
		} catch (SignatureException ex) {
			log.error("Invalid JWT signature");
		} catch (MalformedJwtException ex) {
			log.error("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			log.error("Expired JWT token");
		} catch (UnsupportedJwtException ex) {
			log.error("Unsupported JWT token");
		} catch (IllegalArgumentException ex) {
			log.error("JWT claims string is empty.");
		}
		return false;
	}

}