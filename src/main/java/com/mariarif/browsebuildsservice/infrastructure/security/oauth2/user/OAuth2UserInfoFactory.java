package com.mariarif.browsebuildsservice.infrastructure.security.oauth2.user;

import static com.mariarif.browsebuildsservice.domain.model.AuthProvider.facebook;
import static com.mariarif.browsebuildsservice.domain.model.AuthProvider.google;
import static com.mariarif.browsebuildsservice.domain.model.AuthProvider.yahoo;

import java.util.Map;

import com.mariarif.browsebuildsservice.infrastructure.security.oauth2.exceptions.OAuth2AuthenticationProcessingException;

public class OAuth2UserInfoFactory {
	public static OAuth2UserInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes) {
        if(registrationId.equalsIgnoreCase(google.toString())) {
            return new GoogleOAuth2UserInfo(attributes);
        } else if(registrationId.equalsIgnoreCase(yahoo.toString())) {
            return new YahooOAuth2UserInfo(attributes);
        } else if(registrationId.equalsIgnoreCase(facebook.toString())) {
            return new FacebookOAuth2UserInfo(attributes);
        } else {
            throw new OAuth2AuthenticationProcessingException("Sorry! Login with " + registrationId + " is not supported yet.");
        }
    }
}
