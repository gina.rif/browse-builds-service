package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class PictureExistsException extends BaseException {

	private static final long serialVersionUID = -5797485150811253282L;

	public PictureExistsException(long id) {
		super("Picture with id " +id+" already exists!");
	}
	
}
