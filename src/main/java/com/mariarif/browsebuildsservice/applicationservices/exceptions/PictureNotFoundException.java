package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class PictureNotFoundException extends BaseException {

	private static final long serialVersionUID = -5840511522509448198L;

	public PictureNotFoundException(long id, Throwable t) {
		super("Picture with id " + id + " does not exist!", t);
	}

	public PictureNotFoundException(String msg, Throwable t) {
		super(msg, t);
	}

	public PictureNotFoundException(long id) {
		super("Picture with id " + id + " does not exist!");
	}

	public PictureNotFoundException(String msg) {
		super(msg);
	}

}
