package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class MessageNotFoundException extends BaseException {

	private static final long serialVersionUID = -5841515522509448198L;

	public MessageNotFoundException(long id, Throwable t) {
		super("Message with id " + id + " does not exist!", t);
	}

	public MessageNotFoundException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public MessageNotFoundException(long id) {
		super("Message with id " + id + " does not exist!");
	}

	public MessageNotFoundException(String msg) {
		super(msg);
	}
	
}
