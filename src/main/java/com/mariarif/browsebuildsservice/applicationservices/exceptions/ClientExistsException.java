package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class ClientExistsException extends BaseException {

	private static final long serialVersionUID = -5797485150881223282L;

	public ClientExistsException(long id) {
		super("Client with id " +id+" already exists!");
	}
	
	public ClientExistsException(String email) {
		super("A client account with email " + email + " already exists!");
	}

}
