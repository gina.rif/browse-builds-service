package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class UserNotFoundException extends BaseException {

	private static final long serialVersionUID = -5840515522509448199L;

	public UserNotFoundException(long id, Throwable t) {
		super("User with id " + id + " does not exist!", t);
	}

	public UserNotFoundException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public UserNotFoundException(long id) {
		super("User with id " + id + " does not exist!");
	}

	public UserNotFoundException(String msg) {
		super(msg);
	}
	
}
