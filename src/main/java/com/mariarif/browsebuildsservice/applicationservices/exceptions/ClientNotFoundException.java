package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class ClientNotFoundException extends BaseException {

	private static final long serialVersionUID = -5840515522509448198L;

	public ClientNotFoundException(long id, Throwable t) {
		super("Company with id " + id + " does not exist!", t);
	}

	public ClientNotFoundException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public ClientNotFoundException(long id) {
		super("Company with id " + id + " does not exist!");
	}

	public ClientNotFoundException(String msg) {
		super(msg);
	}
	
}
