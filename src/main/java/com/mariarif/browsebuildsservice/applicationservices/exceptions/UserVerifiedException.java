package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class UserVerifiedException extends BaseException {

	private static final long serialVersionUID = -5797485151881223282L;

	public UserVerifiedException(long id) {
		super("Email of user with id " +id+"is already verified!");
	}
	
	public UserVerifiedException() {
		super("Email of user is already verified!");
	}

}
