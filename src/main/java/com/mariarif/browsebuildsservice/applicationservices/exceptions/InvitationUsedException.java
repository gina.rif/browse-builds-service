package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class InvitationUsedException extends BaseException {

	private static final long serialVersionUID = -1840115502509448198L;
	
	public InvitationUsedException() {
		super("Invitation was already used!");
	}

	public InvitationUsedException(String token) {
		super("Invitation with token " + token + " was already used!");
	}

}
