package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class BaseException extends RuntimeException{
	private static final long serialVersionUID = 1826160578916177803L;
	
	public BaseException(String message) {
		super(message);
	}
	public BaseException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
