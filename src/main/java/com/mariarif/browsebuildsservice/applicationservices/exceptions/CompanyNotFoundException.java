package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class CompanyNotFoundException extends BaseException {

	private static final long serialVersionUID = -5840515502509448198L;

	public CompanyNotFoundException(long id, Throwable t) {
		super("Company with id " + id + " does not exist!", t);
	}

	public CompanyNotFoundException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public CompanyNotFoundException(long id) {
		super("Company with id " + id + " does not exist!");
	}

	public CompanyNotFoundException(String msg) {
		super(msg);
	}
	
}
