package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class InvitationNotFoundException extends BaseException {

	private static final long serialVersionUID = -1840115502509448198L;
	
	public InvitationNotFoundException(Throwable t) {
		super("Invitation does not exist!", t);
	}

	public InvitationNotFoundException(String token) {
		super("Invitation with token " + token + " does not exist!");
	}

}
