package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class CategoryNotFoundException extends BaseException {

	private static final long serialVersionUID = -5840515522509441198L;

	public CategoryNotFoundException(long id, Throwable t) {
		super("Category with id " + id + " does not exist!", t);
	}

	public CategoryNotFoundException(long id) {
		super("Category with id " + id + " does not exist!");
	}

}
