package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class InvalidInvitationException extends BaseException {

	private static final long serialVersionUID = -1840515502509448198L;
	
	public InvalidInvitationException(Throwable t) {
		super("Invitation is invalid!", t);
	}

	public InvalidInvitationException(String token) {
		super("Invitation with token " + token + " is invalid!");
	}

}
