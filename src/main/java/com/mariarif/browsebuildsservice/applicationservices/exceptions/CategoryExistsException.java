package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class CategoryExistsException extends BaseException {

	private static final long serialVersionUID = -5197485150881253282L;

	public CategoryExistsException(long id) {
		super("Category with id " +id+" already exists!");
	}
	

}
