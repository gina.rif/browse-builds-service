package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class InvalidPhoneException extends BaseException {
	
	private static final long serialVersionUID = -4968446171145632249L;

	public InvalidPhoneException(String phone) {
		super(String.format("The following phone number is invalid: %s", phone));
	}
}
