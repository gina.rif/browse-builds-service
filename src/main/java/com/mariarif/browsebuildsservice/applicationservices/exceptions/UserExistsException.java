package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class UserExistsException extends BaseException {

	private static final long serialVersionUID = -5797485151881223282L;

	public UserExistsException(long id) {
		super("User with id " +id+" already exists!");
	}
	
	public UserExistsException(String email) {
		super("An user account with email " + email + " already exists!");
	}

}
