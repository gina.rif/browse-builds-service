package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class PostNotFoundException extends BaseException {

	private static final long serialVersionUID = -5840511522509448198L;

	public PostNotFoundException(long id, Throwable t) {
		super("Post with id " + id + " does not exist!", t);
	}

	public PostNotFoundException(String msg, Throwable t) {
		super(msg, t);
	}

	public PostNotFoundException(long id) {
		super("Post with id " + id + " does not exist!");
	}

	public PostNotFoundException(String msg) {
		super(msg);
	}

}
