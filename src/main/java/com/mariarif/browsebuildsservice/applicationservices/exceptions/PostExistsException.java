package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class PostExistsException extends BaseException {

	private static final long serialVersionUID = -5797485150811253282L;

	public PostExistsException(long id) {
		super("Post with id " +id+" already exists!");
	}
	
}
