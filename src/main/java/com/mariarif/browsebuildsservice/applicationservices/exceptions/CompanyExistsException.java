package com.mariarif.browsebuildsservice.applicationservices.exceptions;

public class CompanyExistsException extends BaseException {

	private static final long serialVersionUID = -5797485150881253282L;

	public CompanyExistsException(long id) {
		super("Company with id " +id+" already exists!");
	}
	
	public CompanyExistsException(String email) {
		super("A company account with email " + email + " already exists!");
	}

}
