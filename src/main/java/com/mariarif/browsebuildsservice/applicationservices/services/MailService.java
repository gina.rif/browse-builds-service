package com.mariarif.browsebuildsservice.applicationservices.services;

import com.mariarif.browsebuildsservice.domain.model.Message;
import com.mariarif.browsebuildsservice.domain.model.User;

public interface MailService {
	boolean sendActivateAccountMail(User user);
	
	boolean sendNewOfferRequestMail(User fromUser, User toUser, Message message);
}
