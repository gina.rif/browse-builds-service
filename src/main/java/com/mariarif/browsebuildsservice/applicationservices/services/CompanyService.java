package com.mariarif.browsebuildsservice.applicationservices.services;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.domain.model.Company;
import com.mariarif.browsebuildsservice.domain.model.Filter;

public interface CompanyService {

	Company createCompany(Company company, long[] categoryIds) throws BaseException;

	Company editCompany(Company company, String phone, MultipartFile profilePicture, long[] categoryIds)
			throws BaseException, IOException;

	void deleteCompanyById(long id) throws BaseException;

	Company getCompanyById(long id) throws BaseException;

	List<Company> getAllCompanies() throws BaseException;

	Page<Company> getAllCompaniesFilteredAndSorted(Filter filter, String sortString, int pageNo, Short elementsPerPage)
			throws BaseException;

	Company getCompanyByEmail(String email) throws BaseException;

	List<Company> getCompaniesLike(String text);
}
