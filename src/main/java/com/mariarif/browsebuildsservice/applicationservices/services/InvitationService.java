package com.mariarif.browsebuildsservice.applicationservices.services;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;

public interface InvitationService {
	
	void useInvitation(String invitationToken) throws BaseException;
	
}