package com.mariarif.browsebuildsservice.applicationservices.services;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.applicationservices.exceptions.UserNotFoundException;
import com.mariarif.browsebuildsservice.domain.model.User;

public interface UserService {
	boolean existsUserWithEmail(String email);
	
	UserDetails loadUserByProviderId(String providerId) throws UsernameNotFoundException;

	UserDetails loadUserByEmail(String email) throws UsernameNotFoundException;

	User getUserById(long id) throws UserNotFoundException;
	
	User getUserByProviderId(String providerId) throws UserNotFoundException;

	User getUserByEmail(String email) throws UserNotFoundException;
	
	User createUser(User user) throws BaseException;

	User editUser(User user) throws BaseException;
	
	void deleteUserById(long id);

	User activateAccount(long id) throws UserNotFoundException;

	void deleteAccount(String providerId);
}
