package com.mariarif.browsebuildsservice.applicationservices.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.domain.model.Client;

public interface ClientService {
	
	Client createClient(Client client) throws BaseException;
	
	Client editClient(Client client, String phone, MultipartFile profilePicture) throws BaseException, IOException;
	
	void deleteClientById(long id) throws BaseException;
	
	Client getClientById(long id) throws BaseException;	
	
	List<Client> getClientsLike(String text);
}