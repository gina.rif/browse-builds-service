package com.mariarif.browsebuildsservice.applicationservices.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.domain.model.Filter;
import com.mariarif.browsebuildsservice.domain.model.Post;

public interface PostService {

	Post createPost(Post post, long companyId, long[] categoriesIds, MultipartFile[] pictures)
			throws BaseException;

	Post editPost(Post post) throws BaseException;

	void deletePostById(long id) throws BaseException;

	Post getPostById(long id) throws BaseException;

	Page<Post> getAllPostsFilteredAndSorted(Filter filter, String sortString, int pageNo, Short elementsPerPage);

	Page<Post> getAllPostsByCompanyId(long companyId, int pageNo, Short elementsPerPage);

	List<Post> getPostsLike(String text);
}