package com.mariarif.browsebuildsservice.applicationservices.services;

import java.io.IOException;
import java.nio.file.Path;

import org.springframework.web.multipart.MultipartFile;

import lombok.NonNull;

public interface FileService {
	Path getFileStorageLocation();

	String uploadFile(@NonNull MultipartFile file, String containerName) throws IOException;

	Boolean uploadAndDownloadFile(@NonNull MultipartFile file, String containerName);

}
