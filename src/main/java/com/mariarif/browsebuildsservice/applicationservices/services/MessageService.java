package com.mariarif.browsebuildsservice.applicationservices.services;

import java.io.InvalidObjectException;
import java.util.List;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.domain.model.Message;

public interface MessageService {

	Message createMessage(Message message, long fromUserId, long toUserId, long[] categoriesIds)
			throws BaseException, InvalidObjectException;

	void deleteMessageById(long id, long userId) throws BaseException;

	Message getMessageById(long id, long userId) throws BaseException;

	List<Message> getAllMessagesByFromUserId(long fromUserId);

}