package com.mariarif.browsebuildsservice.applicationservices.services;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.domain.model.Picture;

import lombok.NonNull;

public interface PictureService {

	Picture addPicture(@NonNull MultipartFile file) throws IOException;

//	Picture editPicture(Picture picture) throws BaseException;

	void deletePictureById(long id) throws BaseException;

	Picture getPictureById(long id) throws BaseException;

//	List<Picture> getAllPicturesByPostId(long postId);

}