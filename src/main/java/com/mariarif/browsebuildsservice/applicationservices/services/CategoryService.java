package com.mariarif.browsebuildsservice.applicationservices.services;

import java.util.List;

import com.mariarif.browsebuildsservice.applicationservices.exceptions.BaseException;
import com.mariarif.browsebuildsservice.domain.model.Category;

public interface CategoryService {

	Category createCategory(Category category) throws BaseException;

	Category editCategory(Category category) throws BaseException;

	void deleteCategoryById(long id) throws BaseException;

	Category getCategoryById(long id) throws BaseException;

	List<Category> getAllCategories();

	List<Category> getCategoriesByIds(List<Long> ids) throws BaseException;

	List<Category> getCategoriesLike(String text);
//	Map<Long, Category> getCategoriesByParentCategoryId(String id) throws BaseException;

}