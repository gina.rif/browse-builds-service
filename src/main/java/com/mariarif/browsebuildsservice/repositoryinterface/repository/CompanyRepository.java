package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mariarif.browsebuildsservice.domain.model.Company;

public interface CompanyRepository extends JpaRepository<Company, Long>{
	
	@Query(value="SELECT c.* FROM companies c, users u WHERE c.id = u.id and u.email = ?1", nativeQuery = true)
	Optional<Company> findByUserWithEmail(String email);
	
	List<Company> findAllByCompanyNameContainingIgnoreCase(String text);
	
	
	@Query(value = "SELECT * FROM COMPANIES WHERE " 
			+ "(:addressesSize = 0 OR user_id IN (SELECT company_user_id FROM addresses WHERE county IN (:counties) AND city IN (:cities))) AND "
			+ "(:categoryIdsSize = 0 OR user_id IN (SELECT company_id FROM companies_categories WHERE category_id IN (:categoryIds)))",
			nativeQuery = true)
	Page<Company> findAllByCategoryIdInAndAddressIn(
			@Param("categoryIds") List<Long> categoryIds,
			@Param("categoryIdsSize") int categoryIdsSize,
			@Param("counties") List<String> counties,
			@Param("cities") List<String> cities,
			@Param("addressesSize") int addressesSize,
			Pageable pageable);
	
//	@Query(value = "SELECT c.user_id, c.company_name FROM companies c", nativeQuery = true)
//	List<Company> getAllAsIdAndCompanyName();
}
