package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mariarif.browsebuildsservice.domain.model.Invitation;

public interface InvitationRepository extends JpaRepository<Invitation, Long> {
	Optional<Invitation> findByToken(String token);
}
