package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mariarif.browsebuildsservice.domain.model.Post;

public interface PostRepository extends JpaRepository<Post, Long> {

	Optional<Post> findPostById(long id);

	List<Post> findPostsByCompanyId(long companyId);

	@Query(value = "SELECT * FROM POSTS WHERE "
			+ "((:dateFrom = '' OR date_posted >= :dateFrom) AND (:dateTo = '' OR date_posted <= :dateTo)) AND "
			+ "(:companyIdsSize = 0 AND :addressesSize = 0 "
				+ "OR :addressesSize = 0 AND company_id IN (:companyIds) "
				+ "OR :companyIdsSize = 0 AND company_id IN (SELECT company_user_id FROM addresses WHERE county IN (:counties) AND city IN (:cities))"
				+ "OR company_id IN (SELECT company_user_id FROM addresses WHERE company_user_id IN (:companyIds) AND  county IN (:counties) AND city IN (:cities))) AND "
			+ "(:categoryIdsSize = 0 OR id IN (SELECT post_id FROM posts_categories WHERE category_id IN (:categoryIds)))",
			nativeQuery = true)
	Page<Post> findAllByDatePostedInAndCompanyIdInAndCategoryIdIn(
			@Param("dateFrom") String dateFrom,
			@Param("dateTo") String dateTo,
			@Param("companyIds") List<Long> companyIds,
			@Param("companyIdsSize") int companyIdsSize,
			@Param("categoryIds") List<Long> categoryIds,
			@Param("categoryIdsSize") int categoryIdsSize,
			@Param("counties") List<String> counties,
			@Param("cities") List<String> cities,
			@Param("addressesSize") int addressesSize,
			Pageable pageable);
	
	List<Post> findAllByDescriptionContainingIgnoreCase(String text);
	
	@Query(value = "SELECT * FROM POSTS WHERE  company_id=:companyId", nativeQuery = true)
	Page<Post> findAllByCompanyId(@Param("companyId") long companyId, Pageable pageable);
}
