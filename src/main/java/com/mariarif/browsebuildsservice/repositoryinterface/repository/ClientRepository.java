package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mariarif.browsebuildsservice.domain.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

	@Query(value = "SELECT user_id, first_name, last_name FROM clients WHERE upper(first_name + ' ' + last_name) LIKE upper(:text) "
			+ "OR upper(last_name + ' ' + first_name) LIKE upper(:text) ORDER BY last_name ASC", nativeQuery = true)
	List<Client> findAllByFullNameContainingIgnoreCase(@Param("text") String text);
}
