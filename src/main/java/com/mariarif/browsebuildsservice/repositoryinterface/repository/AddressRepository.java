package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mariarif.browsebuildsservice.domain.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
