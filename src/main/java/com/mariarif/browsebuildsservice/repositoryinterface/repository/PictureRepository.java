package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mariarif.browsebuildsservice.domain.model.Picture;

public interface PictureRepository extends JpaRepository<Picture, Long> {
	
//	List<Picture> findByPostId(long postId);
}
