package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mariarif.browsebuildsservice.domain.model.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {
	List<Message> findAllByFromUserId(long fromUserId);
}
