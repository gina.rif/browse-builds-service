package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mariarif.browsebuildsservice.domain.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
	Optional<User> findById(long id);
	
	Optional<User> findByProviderId(String providerId);

	Optional<User> findByEmail(String email);
}
