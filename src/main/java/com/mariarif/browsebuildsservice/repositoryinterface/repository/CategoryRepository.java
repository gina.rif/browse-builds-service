package com.mariarif.browsebuildsservice.repositoryinterface.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mariarif.browsebuildsservice.domain.model.Category;
import com.mariarif.browsebuildsservice.domain.model.CategoryAsIdAndNameAndParentCategory;

public interface CategoryRepository extends JpaRepository<Category, Long>{
	
	@Query(value = "SELECT id, name, parent_category_id FROM categories", nativeQuery = true)
	List<CategoryAsIdAndNameAndParentCategory> getIdAndNameAndParentCategory();
	
	List<Category> findAllByNameContainingIgnoreCase(String text);
//	Map<Long,Category> findCategoriesByParentCategoryId(long id);
}
