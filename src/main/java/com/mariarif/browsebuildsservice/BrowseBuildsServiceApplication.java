package com.mariarif.browsebuildsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
//@EnableResourceServer
//@ComponentScan({ "com.maria.browsebuildsservice" })
public class BrowseBuildsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrowseBuildsServiceApplication.class, args);
	}

}
